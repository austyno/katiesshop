<?php

Route::get('/', [
    'uses' => 'ShopController@home',
    'as' => 'shop.home'
]);

Route::group(['prefix' => 'shop'], function () {

    Route::get('/product/detail/{slug}', [
        'uses' => 'ShopController@detail',
        'as' => 'product.detail'
    ]);
    Route::get('/product/{cat}/{name}/{id}', [
        'uses' => 'ShopController@category',
        'as' => 'product.category'
    ]);
    Route::get('/brand/{name}', [
        'uses' => 'ShopController@brand',
        'as'  => 'brand.products'
    ]);
    Route::get('/login', [
        'uses' => 'ShopController@signup',
        'as' => 'shop.login'
    ]);
    Route::get('/track', [
        'uses' => 'ShopController@track_orders',
        'as' => 'shop.track-orders'
    ]);
    Route::post('/track', [
        'uses' => 'ShopController@track',
        'as' => 'track'
    ]);
    Route::post('/search',[
        'uses' => 'ShopController@search',
        'as' => 'shop.search'
    ]);
    Route::get('/search',[
        'uses' => 'ShopController@search',
        'as' => 'shop.search'
    ]);
    Route::get('/history',[
        'uses' => 'ShopController@history',
        'as' => 'shop.history'
    ]);
    Route::post('/history',[
        'uses' => 'ShopController@show_history',
        'as' => 'shop.records'
    ]);
    Route::get('/{provider}/auth',[
        'uses' => 'ShopController@auth',
        'as' => 'social.auth'
    ]);
    Route::get('/{provider}/redirect',[
        'uses' => 'ShopController@auth_callback',
        'as' => 'social.callback'
    ]);

    // shopping cart
    Route::get('/cart/add/{id}', [
        'uses' => 'ShoppingCartController@add_to_cart',
        'as'   => 'cart.add'
    ]);
    Route::post('/cart/add/{id}', [
        'uses' => 'ShoppingCartController@add_to_cart',
        'as'   => 'cart.add'
    ]);
    Route::get('/cart', [
        'uses' => 'ShoppingCartController@show',
        'as' => 'cart'
    ]);
    Route::get('/cart/incr/{id}', [
        'uses' => 'ShoppingCartController@incr',
        'as' => 'cart.incr'
    ]);
    Route::get('/cart/dec/{id}', [
        'uses' => 'ShoppingCartController@decr',
        'as' => 'cart.decr'
    ]);
    Route::get('/cart/remove/{id}', [
        'uses' => 'ShoppingCartController@remove',
        'as' => 'cart.remove'
    ]);

    //checkout
    Route::get('/checkout', [
        'uses' => 'ShoppingCartController@checkout',
        'as' => 'checkout'
    ]);
    
    //paystack
    Route::post('/pay', [
        'uses' => 'PaymentController@redirectToGateway',
        'as' => 'pay'
    ]);
    Route::get('/payment/callback', [
        'uses' => 'PaymentController@handleGatewayCallback',
        'as' => 'pmtcallback'
    ]);
    Route::get('/cashondelivery', [
        'uses' => 'PaymentController@cash_on_delivery',
        'as' => 'cash.delivery'
    ]);
});


Route::group(['prefix' => 'admin'], function () {

    Route::get('/login', [
        'uses' => 'Auth\AdminLoginController@showloginform',
        'as' => 'login.form'
    ]);
    Route::post('/login/submit', [
        'uses' => 'Auth\AdminLoginController@login',
        'as' => 'login.submit'
    ]);
    Route::get('/', [
        'uses' => 'AdminController@index',
        'as' => 'dashboard'
    ]);
    Route::get('/status',[
        'uses' => 'ProductsController@status',
        'as' => 'admin.status'
    ]);
    Route::get('/orders',[
        'uses' => 'ProductsController@allOrders',
        'as' => 'admin.all-orders'
    ]);
    Route::get('/items',[
        'uses' => 'ProductsController@items',
        'as' => 'admin.items'
    ]);

    Route::post('/logout', [
        'uses' => 'Auth\AdminLoginController@logout',
        'as' => 'admin.logout'
    ]);


    //password reset routes
    Route::post('/password/email', [
        'uses' => 'Auth\AdminForgotPasswordController@sendResetLinkEmail',
        'as' => 'admin.password.email'
    ]);

    Route::get('/password/reset', [
        'uses' => 'Auth\AdminForgotPasswordController@showLinkRequestForm',
        'as' => 'admin.password.request'
    ]);

    Route::post('/password/reset', [
        'uses' => 'Auth\AdminResetPasswordController@reset',
        'as' => 'admin.password.update'
        ]);

    Route::get('/password/reset/{token}', [
        'uses' => 'Auth\AdminResetPasswordController@showResetForm',
        'as' => 'admin.password.reset'
    ]);

    //categories
    Route::get('/category/new', [
        'uses' => 'CategoryController@create',
        'as' => 'admin.category.new'
    ]);

    Route::post('/category/submit', [
        'uses' => 'CategoryController@store',
        'as' => 'admin.category.submit'
    ]);
    Route::get('/category/all', [
        'uses' => 'CategoryController@index',
        'as' => 'admin.category.all'
    ]);
    Route::get('/category/edit/{id}', [
        'uses' => 'CategoryController@edit',
        'as' => 'admin.category.edit'
    ]);
    Route::post('/category/update/{id}', [
        'uses' => 'CategoryController@update',
        'as' => 'admin.category.update'
    ]);
    Route::get('/category/newsub', [
        'uses' => 'CategoryController@createsub',
        'as' => 'admin.category.newsubcat'
    ]);
    Route::get('/category/allsubcat',[
        'uses' => 'CategoryController@all_subcat',
        'as' => 'admin.category.allsubcat'
    ]);
    Route::get('/category/subcat/edit/{id}',[
        'uses' => 'CategoryController@edit_subcat',
        'as'  => 'admin.category.editsubcat'
    ]);
    Route::post('/category/subcat/edit/{id}',[
        'uses'  => 'CategoryController@update_subcat',
        'as'   =>  'admin.category.updatesubcat'
    ]);
    Route::get('/category/newsubsub', [
        'uses' => 'CategoryController@createsubsub',
        'as' => 'admin.category.newsubsubcat'
    ]);
    Route::post('/category/newsub', [
        'uses' => 'CategoryController@submitsubcat',
        'as' => 'admin.category.submitsubcat'
    ]);

    //brands
    Route::get('/category/brands', [
        'uses' => 'CategoryController@brands',
        'as'   => 'admin.category.brands'
    ]);
    Route::get('/category/brands/new', [
        'uses' => 'CategoryController@brandnew',
        'as' => 'admin.category.brands.new'
    ]);
    Route::post('/category/brands/new', [
        'uses' => 'CategoryController@brandsave',
        'as' => 'admin.category.brands.new'
    ]);

    //products
    Route::get('/products/new', [
        'uses' => 'ProductsController@create',
        'as' => 'admin.products.new'
    ]);
    Route::post('/products/save', [
        'uses' => 'ProductsController@store',
        'as' => 'admin.products.save'
    ]);
    Route::get('/products/all', [
        'uses' => 'ProductsController@index',
        'as' => 'admin.products.all'
    ]);
    Route::get('/products/edit/{id}', [
        'uses' => 'ProductsController@edit',
        'as' => 'admin.products.edit'
    ]);
    Route::post('/products/update/{id}', [
        'uses' => 'ProductsController@update',
        'as' => 'admin.products.update'
    ]);
    Route::get('/products/viewdetails/{id}', [
        'uses' => 'ProductsController@show',
        'as' => 'admin.products.viewdetails'
    ]);
    Route::get('/products/delete/{id}', [
        'uses' => 'ProductsController@destroy',
        'as' => 'admin.products.delete'
    ]);
    Route::get('/products/trashed', [
        'uses' => 'ProductsController@trashed',
        'as' => 'admin.products.trashed'
    ]);
    Route::get('/products/destroy/{id}', [
        'uses' => 'ProductsController@kill',
        'as' => 'admin.products.destroy'
    ]);
    Route::get('/products/restore/{id}', [
        'uses' => 'ProductsController@restore',
        'as' => 'admin.products.restore'
    ]);

    //customers
    Route::get('/customers',[
        'uses' => 'ProductsController@customers',
        'as' => 'admin.customers'
    ]);
    Route::get('/invoices',[
        'uses' => 'ProductsController@invoices',
        'as' => 'admin.invoices'
    ]);
    Route::get('/unpaid',[
        'uses' => 'ProductsController@unpaid',
        'as' => 'admin.unpaid'
    ]);
});
//ajax
Route::post('/ajaxcat ', [
    'uses' => 'CategoryController@ajaxcat',
    'as' => 'admin.category.ajaxcat'
]);
Route::post('/ajaxsubcat', [
    'uses' => 'CategoryController@ajaxsubcat',
    'as' => 'admin.category.ajaxsubcat'
]);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
