@extends('shop.layout.main')

@section('content')
<style>
.shopcat{
    border-radius: 0px;
    color: #333;
    font-size: 14px;
    font-family: 'Open Sans', sans-serif;
    padding: 12px 17px;
    text-transform: uppercase;
    background-color: #fdd922;
    border: 1px solid #e9c532;
    font-weight: 700;
    letter-spacing: 0.5px;
    border-bottom: 1px #f1ce3c solid; 
    width:100%;
}

</style>
<div class="breadcrumb">
    <div class="container">
        <div class="breadcrumb-inner">
        <ul class="list-inline list-unstyled">
            <li><a href="{{route('shop.home')}}">Home</a></li>
            {{-- <li><a href="#">{{$cat}}</a></li> --}}
            <li class='active'>{{$q}}</li>
        </ul>
        </div>
        <!-- /.breadcrumb-inner --> 
    </div>
    <!-- /.container --> 
</div>


<div class='row'>
        <div class='col-md-3 sidebar'> 
          <div class="sidebar-module-container">
            <div class="sidebar-filter"> 
                <div class="shopcat"><i class="icon fa fa-align-justify fa-fw"></i>&nbsp; shop by</div>
              <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
              <div class="sidebar-widget wow fadeInUp">
                  <div class="widget-header">
                    <h4 class="section-title">Category</h4>
                  </div>
                  <div class="sidebar-widget-body">
                    <div class="accordion">
                      <div class="accordion-group">
                          <?php $count = 1; $cnt =300; ?>
                          @foreach ($menu as $m)
                            <div class="accordion-heading"><a href="#collapse{{$count}}" data-toggle="collapse" class="accordion-toggle collapsed"> {{$m->name}}</a></div>
                            <div class="accordion-body collapse" id="collapse{{$count}}" style="height: 0px;">
                              <div class="accordion-inner">
                                <ul>
                                  @foreach ($m->subcategories as $sub)
                                    @if($sub->subsubcategories->count() > 0)
                                    <div class="accordion-heading" style=""> <a href="#collapse{{$cnt}}" data-toggle="collapse" class="accordion-toggle collapsed">{{$sub->name}}</a> </div>
                                    <div class="accordion-body collapse" id="collapse{{$cnt}}" style="height: 0px;margin-left:7px">
                                    <div class="accordion-inner">
                                      <ul>
                                        @foreach ($sub->subsubcategories as $subsub)
                                          <li><a href="{{route('product.category',['cat'=>'subsubcategory','name' => $subsub->name,'id' =>$subsub->id])}}">{{$subsub->name}}</a></li>
                                        @endforeach
                                        <?php $cnt++ ?>
                                      </ul>
                                    </div> 
                                  </div>
                                    @else
                                      <li><a href="{{ route('product.category',['cat'=>'subcategory','name' => $sub->name,'id' =>$sub->id])}}">{{$sub->name}}</a></li>
                                    @endif                                
                                    <?php $count++ ?>
                                  @endforeach
                                </ul>
                              </div>
                                
                            </div>
                          @endforeach
                      </div>
                      <!-- /.accordion-group -->
                    </div>
                    <!-- /.accordion --> 
                  </div>
                  <!-- /.sidebar-widget-body --> 
                </div>
              <!-- /.sidebar-widget --> 
              <!-- ============================================== SIDEBAR CATEGORY : END ============================================== --> 
            
              <!-- ============================================== MANUFACTURES============================================== -->
              <div class="shopcat" style="margin-top: 30px;"><i class="icon fa fa-align-justify fa-fw"></i>&nbsp; shop by</div>
              <div class="sidebar-widget wow fadeInUp head" >
                <div class="widget-header">
                  <h4 class="section-title">Brand</h4>
                </div>
                <div class="sidebar-widget-body">
                  <ul class="list">
                    @foreach($brands as $brand)
                      <li><a href="{{ route('brand.products',['name' => $brand->name]) }}">{{$brand->name}} <span class="pull-right" style="font-weight:700;letter-spacing:0.5px;color: #333;font-size: 14px;font-family: 'Open Sans', sans-serif;padding: 5px 5px;">{{$brand->products->count()}}</span></a></li>
                    @endforeach
                  </ul> 
                </div>
                <!-- /.sidebar-widget-body --> 
              </div>
              <!-- /.sidebar-widget --> 
              <!-- ============================================== MANUFACTURES: END ============================================== --> 
             
              
              <!-- /.sidebar-widget --> 
            <!----------- Testimonials------------->
              <div class="sidebar-widget  wow fadeInUp outer-top-vs ">
                <div id="advertisement" class="advertisement">
                  <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member1.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">John Doe <span>Abc Company</span> </div>
                    <!-- /.container-fluid --> 
                  </div>
                  <!-- /.item -->
                  
                  <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member3.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em>Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">Stephen Doe <span>Xperia Designs</span> </div>
                  </div>
                  <!-- /.item -->
                  
                  <div class="item">
                    <div class="avatar"><img src="assets/images/testimonials/member2.png" alt="Image"></div>
                    <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>
                    <div class="clients_author">Saraha Smith <span>Datsun &amp; Co</span> </div>
                    <!-- /.container-fluid --> 
                  </div>
                  <!-- /.item --> 
                  
                </div>
                <!-- /.owl-carousel --> 
              </div>
              
              <!-- ============================================== Testimonials: END ============================================== -->
              
              {{-- <div class="home-banner"> <img src="assets/images/banners/LHS-banner.jpg" alt="Image"> </div> mobile app image & download link --}}
            </div>
            <!-- /.sidebar-filter --> 
          </div>
          <!-- /.sidebar-module-container --> 
        </div>
        <!-- /.sidebar -->
        <div class='col-md-9'> 
          <!-- ========================================== SECTION – HERO ========================================= -->
          
          <div class="clearfix filters-container m-t-10" style="margin-top:-3px">
            <div class="row">
              <div class="col col-sm-6 col-md-2">
                <div class="filter-tabs">
                  <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                    <li class="active"> <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-large"></i>Grid</a> </li>
                    <li><a data-toggle="tab" href="#list-container"><i class="icon fa fa-th-list"></i>List</a></li>
                  </ul>
                </div>
                <!-- /.filter-tabs --> 
              </div>
              <!-- /.col -->
              <div class="col col-sm-12 col-md-6">
                <div class="col col-sm-3 col-md-6 no-padding">
                  <div class="lbl-cnt"> <span class="lbl"></span>
                    <!-- /.fld --> 
                  </div>
                  <!-- /.lbl-cnt --> 
                </div>
                <!-- /.col -->
                <div class="col col-sm-3 col-md-6 no-padding">
                  <div class="lbl-cnt"> <span class="lbl"></span>
                    <div class="fld inline">
                    </div>
                    <!-- /.fld --> 
                  </div>
                  <!-- /.lbl-cnt --> 
                </div>
                <!-- /.col --> 
              </div>
              <!-- /.col -->
              <div class="col col-sm-6 col-md-4 text-right">
                <div class="text-right">
                  {{$result->links()}}
                  <!-- /.list-inline --> 
                </div>
                <!-- /.pagination-container --> </div>
              <!-- /.col --> 
            </div>
            <!-- /.row --> 
          </div>
          <div class="search-result-container ">
              <div id="myTabContent" class="tab-content category-list">
                <div class="tab-pane active " id="grid-container">
                  <div class="category-product">
                    <div class="row">
                      
                      @foreach ($result as $cp)
                        <div class="col-sm-6 col-md-3 wow fadeInUp group"  style="height:249px;margin-bottom:20px">
                          <div class="products" >
                            <div class="product">
                              <div class="product-image">
                                <div class="image">
                                  <a href="{{ route('product.detail',['slug' => $cp->slug])}}">
                                    <img class="group"  src="{{asset($cp->image)}}" alt="" height="150px">
                                  </a> 
                                </div>
                                <!-- /.image -->
                                @if($cp->discount)
                                  <div class="tag new"><span>-{{$cp->discount}}%</span></div>
                                @endif
                                
                              </div>
                              <!-- /.product-image -->
                              
                              <div class="product-info text-left">
                                <h3 class="name"><a href="{{ route('product.detail',['slug' => $cp->slug])}}">{{$cp->name}}</a></h3>
                                <div class="rating rateit-small"></div>
                                <div class="description"></div>
                                <div class="product-price">
                                  
                                  @if ($cp->discount)
                                    <span class="price"> ₦ {{$cp->discount_price}} </span>
                                    <span class="price-before-discount">{{$cp->formated_price}}</span>
                                  @else
                                    <span class="price"> ₦ {{$cp->formated_price}} </span>
                                  @endif 
                                   
                                </div>
                                <!-- /.product-price --> 
                                
                              </div>
                              <!-- /.product-info -->
                              <div class="cart clearfix animate-effect">
                                <div class="action">
                                  <ul class="list-unstyled">
                                      <li class="add-cart-button btn-group">
                                          <a href="{{ route('cart.add',['id' => $cp->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>
                                        {{-- <button class="btn btn-primary cart-btn" type="button">Add to cart</button> --}}
                                      </li>
                                    <li class="lnk wishlist"> <a class="add-to-cart" href="{{ route('product.detail',['slug' => $cp->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{ asset($cp->image) }}" data-fancybox data-caption="{{$cp->name}} &nbsp;&nbsp; Price <?php if($cp->discount){ echo '₦'.$cp->discount_price;}else{ echo '₦'.$cp->formated_price;} ?> " title="Quick View"> <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                                      </li>
                                  </ul>
                                </div>
                                <!-- /.action --> 
                              </div>
                              <!-- /.cart --> 
                            </div>
                            <!-- /.product --> 
                            
                          </div>
                          <!-- /.products --> 
                        </div>
                        
                      @endforeach 
                    </div>
                    <!-- /.row --> 
                  </div>
                  <!-- /.category-product --> 
                  
                </div>
                <!-- /.tab-pane -->
                
                <div class="tab-pane "  id="list-container">
                  <div class="category-product">
                    @foreach ($result as $c) 
                      <div class="category-product-inner wow fadeInUp">
                        <div class="products">
                          <div class="product-list product">
                            <div class="row product-list-row">
                              <div class="col col-sm-4 col-lg-4">
                                <div class="product-image">
                                  <div class="image"> <img src="{{ asset($c->image)}}" alt="" height="150px" width="150px"> </div>
                                </div>
                                <!-- /.product-image --> 
                              </div>
                              <!-- /.col -->
                              <div class="col col-sm-8 col-lg-8">
                                <div class="product-info">
                                  <h3 class="name"><a href="{{ route('product.detail',['slug' => $c->slug])}}">{{$c->name}}</a></h3>
                                  <div class="rating rateit-small"></div>
                                  <div class="product-price">
                                    <?php 
                                       //$priceAfterDiscount = $c->price - (($c->discount/100)*$c->price)
                                    ?>
                                    @if ($c->discount)
                                      <span class="price">₦ {{$c->discount_price}}</span> 
                                      <span class="price-before-discount"> ₦ {{$c->formated_price}}</span>
                                    @else
                                      <span class="price">₦ {{$c->formated_price}}</span> 
                                    @endif 
                                     
                                  </div>
                                  <!-- /.product-price -->
                                  <?php $text = $c->description ?>
                                <div class="description m-t-10">{!!substr($text,0,150)!!}</div>
                                  <div class="cart clearfix animate-effect">
                                    <div class="action">
                                      <ul class="list-unstyled">
                                          <li class="add-cart-button btn-group">
                                              <a href="{{ route('cart.add',['id' => $c->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>
                                            {{-- <button class="btn btn-primary cart-btn" type="button">Add to cart</button> --}}
                                          </li>
                                        <li class="lnk wishlist"> <a class="add-to-cart" href="{{ route('product.detail',['slug' => $c->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                                        <li class="lnk">
                                          <a class="add-to-cart" href="{{ asset($c->image) }}" data-fancybox="" data-caption="{{$c->name}} &nbsp;&nbsp; Price <?php if($c->discount){ echo '₦'.''.$c->discount_price;}else{ echo '₦'.$c->formated_price;} ?> " title="Quick View"> <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                                        </li>
                                      </ul>
                                    </div>
                                    <!-- /.action --> 
                                  </div>
                                  <!-- /.cart --> 
                                  
                                </div>
                                <!-- /.product-info --> 
                              </div>
                              <!-- /.col --> 
                            </div>
                            <!-- /.product-list-row -->
                            @if($c->discount)
                            <div class="tag new"><span>-{{$c->discount}}%</span></div>
                            @endif
                            
                          </div>
                          <!-- /.product-list --> 
                        </div>
                        <!-- /.products --> 
                      </div>
                    @endforeach   
                  </div>
                  <!-- /.category-product --> 
                </div>
                <!-- /.tab-pane #list-container --> 
              </div>
              <!-- /.tab-content -->
              <div class="clearfix filters-container">
                <div class="text-right">
                    {{$result->links()}}
                </div>
                <!-- /.text-right --> 
                
              </div>
              <!-- /.filters-container --> 
              
            </div>
          <!-- /.search-result-container --> 
          
        </div>
        <!-- /.col --> 
      </div>

@endsection