<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Cash On Delivery Order</title>
  <style>
    .ord{
      font-weight:700;
      font-size: 14px;
    }
  </style>
</head>
<body>
  <p>Dear {{Auth::user()->name}}</p>
  <h4>Your Order details</h4>
      <table class="table table-striped wow fadeInUp" style="margin-bottom:50px">
          <thead>
            <tr>
              <th>SN</th>
              <th>Image</th>
              <th>Product Name</th>
              <th>Product Price</th>
              <th>Discount Applied</th>
            </tr>
          </thead>
          <tbody>
            <?php $count = 1; ?>
            @foreach (Cart::getContent() as $item)
              <tr>
              <td>{{$count}}</td>
              <td><img src="{{ asset($item->attributes['image'])}}" height="50px" with="70px"></td>
              <td class="ord">{{$item->name}}</td>
              <td class="ord">₦ {{number_format($item->price,2)}}</td>
              <td class="ord">{{$item->attributes['discount']}}%</td>
              </tr><br><br> 
              <?php $count++; ?> 
            @endforeach
            <tr>
              <td>Order Total (7.2% VAT inclusive)</td>
              <td>₦ {{number_format(Cart::getTotal())}}</td>
            </tr>
            <tr>
              <td>Tracking Code</td>
              <td>{{$cust['tracking_code']}}</td>
            </tr>
            <tr>
              <td>Invoice Number</td>
              <td>{{$cust['invoice']}}</td>
            </tr>
        </table>
</body>
</html>