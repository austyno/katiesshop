<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Purchase Successful</title>
</head>
<body>
  <h2>Thank you for your patronage</h2>
  <p>Dear {{$data['name']}}</p>
  <p>Bellow is a summary of your purchases</p>
  <p>Invoice number: {{$data['invoice']}}<p>
  <p>Tracking Code: {{ $data['tracking_code']}}<p>
  <p>Total: ₦ {{number_format($data['total'],2)}}</p>
  <p>You will recieve a call from one of our staff shortly.<br>Thank you </p>

  <p>Katty Kiddies</p>
  <p>Admin</p>
</body>
</html>