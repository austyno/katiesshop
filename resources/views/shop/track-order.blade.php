@extends('shop.layout.main')

@section('content')
<style>
  .ord{
    font-weight:700;
    font-size: 14px;
  }
</style>
<div class="breadcrumb">
    <div class="container">
      <div class="breadcrumb-inner">
        <ul class="list-inline list-unstyled">
          <li><a href="{{ route('shop.home') }}">Home</a></li>
          <li class='active'>Track your orders</li>
        </ul>
      </div><!-- /.breadcrumb-inner -->
    </div><!-- /.container -->
  </div>

  <div class="track-order-page">
		<div class="row">
          <div class="col-md-12">
          <h2 class="heading-title">Track your Order</h2>
          <span class="title-tag inner-top-ss">Please enter your <b><em>Tracking Code</em></b> in the box below and press Enter. This was given to you in the confirmation email you should have received. </span>
          <form class="register-form outer-top-xs" role="form" action="{{ route('track',) }}" method="POST">
            @csrf
            <div class="form-group">
              <label class="info-title" for="exampleOrderId1">Invoice Number</label>
              <input type="text" class="form-control unicase-form-control text-input" name="code" id="exampleOrderId1" placeholder="eg kK123456ABCD">
            </div>
          {{-- <div class="form-group">
            <label class="info-title" for="exampleBillingEmail1">Email</label>
            <input type="email" class="form-control unicase-form-control text-input" id="exampleBillingEmail1" >
          </div> --}}
          <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Track</button>
      </form>	
    </div>			
  </div><!-- /.row -->  
</div>


@if (isset($ord))
<h4 class="text-center" style="margin-top:30px"><i><u>Order Summary</u></i></h4>
<table class="table table-striped wow fadeInUp">
    <tr>
      <th style="width:150px">Invoice Number</th>
      <td class="ord">{{$ord[0]->invoice_number}}</td>
    <tr>
    <tr>
      <th>Order Date</th>
      <td class="ord">{{$ord[0]->created_at}} - <i>{{$ord[0]->created_at->diffForHumans()}}</i></td>
    <tr>
    <tr>
      <th>Order Total</th>
      <td class="ord">₦ {{number_format($ord[0]->total,2)}}</td>
    <tr>
    <tr>
      <th>Order Status</th>
        @if($ord[0]->status == 'undelivered')
          <td class="ord" style="color:tomato">{{$ord[0]->status}}</td>
        @else
          <td class="ord" style="color:green">{{$ord[0]->status}}</td>
        @endif
      
    <tr>
    </table>

    <h4 class="text-center" style="margin-top:30px"><i><u>Order Items</u></i></h4>
    <table class="table table-striped wow fadeInUp" style="margin-bottom:50px">
      <thead>
        <tr>
          <th>SN</th>
          <th>Image</th>
          <th>Product Name</th>
          <th>Product Price</th>
          <th>Discount Applied</th>
        </tr>
      </thead>
      <tbody>
        <?php $count = 1; ?>
        @foreach ($ord[0]->orderItems as $item)
          <tr>
          <td>{{$count}}</td>
          <td><img src="{{asset($item->product->image)}}" height="100px" with="150px"></td>
          <td class="ord">{{$item->product->name}}</td>
          <td class="ord">₦ {{number_format($item->price,2)}}</td>
          <td class="ord">{{$item->product->discount}}%</td>
          </tr> 
          <?php $count++; ?> 
        @endforeach
        <tr></tr>
      </tbody>
  </table>
    
@endif
    
@endsection