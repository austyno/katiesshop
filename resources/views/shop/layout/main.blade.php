<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="An online shop for babies, children, mums to be and mums">
<meta name="author" content="">
<meta name="keywords" content="eCommerce, babyshop, baby shops,children cloths, children toys, toys, back to school">
<meta name="robots" content="all">
<title>Katies Shop</title>

<link rel="icon" href="../../../logo.png" type="image/ico" />
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{ asset('shop/assets/css/bootstrap.min.css')}}">

<!-- Customizable CSS -->
<link rel="stylesheet" href="{{ asset('shop/assets/css/main.css')}}">
<link rel="stylesheet" href="{{ asset('shop/assets/css/blue.css')}}">
<link rel="stylesheet" href="{{ asset('shop/assets/css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{ asset('shop/assets/css/owl.transitions.css')}}">
<link rel="stylesheet" href="{{ asset('shop/assets/css/animate.min.css')}}">
<link rel="stylesheet" href="{{ asset('shop/assets/css/rateit.css')}}">
<link rel="stylesheet" href="{{ asset('shop/assets/css/bootstrap-select.min.css')}}">
<link href="{{ asset('shop/assets/css/toastr.min.css') }}" rel="stylesheet">

<!-- Icons/Glyphs -->
<link rel="stylesheet" href="{{ asset('shop/assets/css/font-awesome.css')}}">

<!-- Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" rel="stylesheet" />

<style>
	.nav a:hover{
		background-color:white !important;
		color:black !important;
	}
	ul li a:hover{
		background:white;
		color:black;
  }
  .footer .footer-bottom .module-body ul li a:hover,
  .footer .footer-bottom .module-body ul li a:focus {
    color: #12cca7;
    background-color:#202020 !important;
  }
	.list-unstyled a:hover{
		background-color:#157ed2 !important;
		color:white !important;
		font-size:13px;
    }
  .list-unstyled .gym a:hover{
		/* background-color:#157ed2 !important; */
		color:white !important;
		font-size:13px;
    }
	.navbar-right a:hover{
        background:#157ed2 !important;
        color:white !important;
    }
  /* .image img{
    width:90px;
    height:60px;
    border-radius:40%;
    text-align: center
  } */
  #brand-slider img{
    width:70px;
    height:70px;
    border-radius: 15px;
  }
</style>
</head>
<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled gym">
            {{-- <li><a href="#"><i class="icon fa fa-heart"></i>Wishlist</a></li> --}}
            <li><a href="{{ route('cart')}}"><i class="icon fa fa-shopping-cart"></i>My Cart</a></li>
            <li><a href="{{ route('checkout')}}"><i class="icon fa fa-check"></i>Checkout</a></li>
            @if (!Auth::check())
            <li><a href="{{ route('shop.login') }}"><i class="icon fa fa-lock"></i>Login</a></li> 
            @endif
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        <div class="cnt-block">
          <ul class="list-unstyled list-inline">
          <li class="dropdown dropdown-small"> 
            @auth
              <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"><span class="value"><i class="icon fa fa-user"></i> My Account</span><b class="caret"></b></a>
              <ul class="dropdown-menu" style="width:auto">
                <li><a href="{{ route('shop.history') }}"><i class="icon fa fa-history"></i>&nbsp;&nbsp; Account History</a></li>
                <li><a href="{{ route('shop.track-orders')}}"><i class="icon fa fa-binoculars"></i>&nbsp;&nbsp; Track an Order</a></li>
                <li>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="fa fa-sign-out"></i> {{ __('Logout') }}
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                  </a>
                </li>
              </ul>  
            @endauth
          </li>
          </ul>
          <!-- /.list-unstyled --> 
        </div>
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header" style="padding-bottom:0px">
    <div class="container">
      <div class="row" style="">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder" style="margin-top:0px"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo" style=""> 
            <a href="{{route('shop.home')}}"> 
              <img src="{{ asset('shop/assets/images/logo.png')}}" alt="logo" style="width:80px;"> 
            </a> 
          </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> </div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder" style="margin-bottom:-50px;"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          <div class="search-area">
          <form method="POST" action="{{route('shop.search')}}">
            @csrf
              <div class="control-group">
                <ul class="categories-filter animate-dropdown">
                  <li class="dropdown"> <a class="dropdown-toggle"  data-toggle="dropdown" href="#">Categories <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu" >
                      @foreach ($menu as $cat)
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('product.category',['cat'=>'category','name' => $cat->name,'id' =>$cat->id])}}">{{ $cat->name }}</a></li>
                      @endforeach
                    </ul>
                  </li>
                </ul>
                <input type="text" name="q" class="search-field" placeholder="Search here..." />
                <button class="search-button" type="submit" ></button> </div>
            </form>
          </div>
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> </div>
        <!-- /.top-search-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row"> 
          <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
          
          <div class="dropdown dropdown-cart"> <a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown" style="width:auto">
            <div class="items-cart-inner">
              <div class="basket" style="width:auto"> <i class="glyphicon glyphicon-shopping-cart"></i> </div>
              <div class="basket-item-count"><span class="count">{{Cart::getContent()->count()}}</span></div>
            <div class="total-price-basket"> <span class="lbl">cart-</span> <span class="total-price" style="width:100%"><span class="value" style="">{{number_format(Cart::getSubTotal(),2)}}</span> </span> </div>
            </div>
            </a>
            <ul class="dropdown-menu">
              <li>
                <div class="cart-item product-summary">
                  @foreach (Cart::getContent() as $c)
                    <div class="row">
                      <div class="col-xs-4">
                        <div class="image"> <a href="{{ route('product.detail',['slug' =>$c->attributes['slug']])}}"><img src="{{($c->attributes['image'])}}" alt=""></a> </div>
                      </div>
                      <div class="col-xs-7">
                        <h3 class="name"><a href="{{ route('product.detail',['slug' =>$c->attributes['slug']])}}">{{$c->name}}</a></h3>
                        <h3 class="name">Qty:&nbsp;{{$c->quantity}}</h3>
                        <div class="price">
                          @if($c->attributes['discount'])
                            ₦{{number_format($c->getPriceWithConditions(),2)}}
                          @else
                            ₦{{number_format($c->price)}}
                          @endif
                        </div>
                      </div>
                      <div class="col-xs-1 action"> <a href="{{ route('cart.remove',['id' => $c->id])}}"><i class="fa fa-trash"></i></a> </div>
                    </div>
                  @endforeach
                </div>
                <!-- /.cart-item -->
                <div class="clearfix"></div>
                <hr>
                <div class="clearfix cart-total">
                
                    <div class="pull-right"> <span class="text">Sub Total :</span><span class='price'>₦{{number_format(Cart::getSubTotal(),2)}}</span> </div>
                    <div class="clearfix"></div>
                    @if(Auth::check())
                      <a href="{{route('checkout')}}" class="btn btn-upper btn-primary btn-block m-t-20">Checkout</a> 
                    @else
                      <a href="{{route('cart')}}" class="btn btn-upper btn-primary btn-block m-t-20">Cart</a> 
                    @endif
                    
                </div>
                <!-- /.cart-total--> 
                
              </li>
            </ul>
            <!-- /.dropdown-menu--> 
          </div>
          <!-- /.dropdown-cart --> 
          
          <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= --> </div>
        <!-- /.top-cart-row --> 
      </div>
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  <div class="header-nav animate-dropdown" style="background:#157ed2">
    <div class="container">
      <div class="yamm navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
          <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="nav-bg-class">
          <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
            <div class="nav-outer">
              <ul class="nav navbar-nav">
              <li class="active dropdown yamm-fw"> <a href="{{ route('shop.home')}}">Home</a> </li>
                @foreach ($menu as $m)
                  <li class="dropdown yamm mega-menu"> <a href="" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">{{ $m->name}}</a> 
                    <ul class="dropdown-menu container">
                      @foreach($m->subcategories as $sub)
                        <li>
                            <div class="yamm-content pull-left" style="padding:5px">
                                <div class="row">
                                  <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                      @if (count($sub->subsubcategories)>0)
                                        <h2 class="title" style="">{{$sub->name}}</h2> 
                                      @else
                                      <a href="{{route('product.category',['cat'=>'subcategory','name' => $sub->name,'id' =>$sub->id])}}" style="padding:0px">{{$sub->name}}</a>
                                      @endif
                                    {{-- <h2 class="title" style="">{{$sub->name}}</h2> --}}
                                    <ul class="links">
                                      @foreach ($sub->subsubcategories as $subsub)
                                      <li><a href="{{route('product.category',['cat'=>'subsubcategory','name' => $subsub->name,'id' =>$subsub->id])}}" style="">{{$subsub->name}}</a></li>
                                      @endforeach
                                    </ul>
                                  </div>
                                </div>
                            </div>
                        </li>
                      @endforeach
                    </ul>
                  </li>     
                @endforeach
              </ul>
                
            </div>
        
              </li>
              </ul>
              <!-- /.navbar-nav -->
              <div class="clearfix"></div>
            </div>
            <!-- /.nav-outer --> 
          </div>
          <!-- /.navbar-collapse --> 
          
        </div>
        <!-- /.nav-bg-class --> 
      </div>


      <!-- /.navbar-default --> 
    </div>
    <!-- /.container-class --> 
    
  </div>
  <!-- /.header-nav --> 
  <!-- ============================================== NAVBAR : END ============================================== --> 
  
</header>

<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content outer-top-xs" id="top-banner-and-menu">
  <div class="container">
    <div class="row"> 
      <!-- ============================================== SIDEBAR ============================================== -->
      
      <!-- /.sidemenu-holder --> 
      <!-- ============================================== SIDEBAR : END ============================================== --> 
      
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
          @yield('content')

     </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <h4 class="text-center" style="margin:20px"><b>Shop by Brand</b></h4>
      <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
          @foreach ($brands as $br)
            <div class=" m-t-15"> 
              <a href="{{ route('brand.products',['name' => $br->name]) }}" class="image"> 
                <img data-echo="{{ asset($br->image)}}" src="asset/images/blank.gif" alt=""> 
              </a> 
            </div>
          <!--/.item-->
          @endforeach
        </div>
        <!-- /.owl-carousel #logo-slider --> 
      </div>
      <!-- /.logo-slider-inner --> 
      
    </div>
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>
<!-- /#top-banner-and-menu --> 

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="module-heading">
            <h4 class="module-title">Contact Us</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class="toggle-footer" style="">
              <li class="media">
                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body">
                  <p><a class='list-unstyled'>By Ushafa Junction, after Ushafa Bridge along Bwari Old Road, Abuja Nigeria.</a></p>
                </div>
              </li>
              <li class="media">
                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body">
                  <p><a class='list-unstyled'>+2347063535121 &nbsp;
                    +2348126590740</a></p>
                </div>
              </li>
              <li class="media">
                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body"> <span><a >info@kattykiddies.com</a></span> </div>
              </li>
            </ul>
          </div>
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        @auth
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="module-heading">
              <h4 class="module-title">Customer Service</h4>
            </div>
            <!-- /.module-heading -->
            
            <div class="module-body">
              <ul class='list-unstyled'>
                
                  <li><a href="{{ route('shop.history') }}"> Account History</a></li>
                  <li><a href="{{ route('shop.track-orders')}}"> Track an Order</a></li>  
                
                {{-- <li><a href="#" title="faq">FAQ</a></li> --}}
              </ul>
            </div>
            <!-- /.module-body --> 
          </div>
        @endauth
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="module-heading">
            <h4 class="module-title">Corporation</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class='list-unstyled'>
              <li class="first"><a title="Your Account" href="#">About us</a></li>
              <li><a title="Information" href="#">Services</a></li>
              <li class="last"><a title="Orders History" href="#">Contact Us</a></li>
            </ul>
          </div>
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
      </div>
    </div>
  </div>
  <div class="copyright-bar">
    <div class="container">
      <div class="col-xs-12 col-sm-6 no-padding social">
        <ul class="link">
          <li class="fb pull-left"><a target="_blank" rel="nofollow" href="https://www.facebook.com/kattykiddiesworld" title="Facebook"></a></li>
          <li class="tw pull-left"><a target="_blank" rel="nofollow" href="https://twitter.com/kattykiddies" title="Twitter"></a></li>
          <li class="insta pull-left"><a target="_blank" rel="nofollow" href="https://www.instagram.com/kattykiddies/" title="instagram"></a></li>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-6 no-padding">
        <div class="clearfix payment-methods">
          <ul>
            <li><img src="{{ asset('shop/assets/images/payments/card-pay.png')}}" alt=""></li>
          </ul>
        </div>
        <!-- /.payment-methods --> 
      </div>
    </div>
  </div>
  <!-- Your customer chat code -->
<div class="fb-customerchat"
attribution=setup_tool
page_id="104568354223502"
logged_in_greeting="Hi!welcome to Katie & A kiddies World,How can we help you?"
logged_out_greeting="Hi!welcome to Katie & A kiddies World,How can we help you?">
</div>  
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

<!-- For demo purposes – can be removed on production --> 

<!-- For demo purposes – can be removed on production : End --> 

<!-- JavaScripts placed at the end of the document so the pages load faster --> 
<script src="{{ asset('shop/assets/js/jquery-1.11.1.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/owl.carousel.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/bootstrap.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="{{ asset('shop/assets/js/bootstrap-hover-dropdown.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/echo.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/jquery.easing-1.3.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/bootstrap-slider.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/jquery.rateit.min.js')}}"></script> 
<script type="text/javascript" src="{{ asset('shop/assets/js/lightbox.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/bootstrap-select.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/wow.min.js')}}"></script> 
<script src="{{ asset('shop/assets/js/scripts.js')}}"></script>
<script src="{{ asset('shop/assets/js/jquery.matchHeight.js')}}" type="text/javascript"></script>
<!-- Toastr -->
<script src=" {{ asset('shop/assets/js/toastr.min.js')}}"></script>
<script>
    toastr.options = {
    "positionClass": "toast-bottom-right"
  }
  
    @if(Session::has('success'))
      toastr.success("{{ Session::get('success') }}")
    @endif

    @if(Session::has('info'))
      toastr.info("{{ Session::get('info') }}")
    @endif
  </script>
  <!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v4.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>