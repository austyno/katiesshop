@extends('shop.layout.main')

@section('content')
<style>
  .ord{
    font-weight:700;
    font-size: 14px;
  }
</style>
<div class="breadcrumb">
    <div class="container">
      <div class="breadcrumb-inner">
        <ul class="list-inline list-unstyled">
          <li><a href="{{ route('shop.home') }}">Home</a></li>
          <li class='active'>Account History</li>
        </ul>
      </div><!-- /.breadcrumb-inner -->
    </div><!-- /.container -->
  </div>

  <div class="track-order-page">
		<div class="row">
          <div class="col-md-12">
          <h2 class="heading-title">Account History</h2>
          
          <form class="register-form outer-top-xs" role="form" method="post" action="{{ route('shop.records') }}">
            @csrf
            <div class="form-group">
              <label class="info-title" for="exampleOrderId1">start</label>
              <input type="date" class="form-control unicase-form-control text-input" name="from" id="exampleOrderId1">
            </div>
          <div class="form-group">
            <label class="info-title" for="exampleBillingEmail1">Ending</label>
            <input type="date" class="form-control unicase-form-control text-input" name="ending">
          </div>
          <button type="submit" class="btn-upper btn btn-primary checkout-page-button">View</button>
      </form>	
    </div>			
  </div><!-- /.row -->  
</div>

@if (isset($hist))
    
    <h4 class="text-center" style="margin-top:30px"><i><u><strong>Cash Orders</strong></u></i></h4>

    @if ($hist->isEmpty())
      <p class="ord text-center">{{'No available record found'}}</p>
    @else
    <table class="table table-striped wow fadeInUp" style="margin-bottom:50px">
      <thead>
        <tr>
          <th>SN</th>
          <th>Date</th>
          <th>Tracking Code</th>
          <th>Invoice Number</th>
          <th>Invoice Amount</th>
          <th>Payment Status</th>
          <th>Delivery Status</th>
        </tr>
      </thead>
      <tbody>

          @foreach ($hist as $hs)
            <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$hs->created_at}}&nbsp; &nbsp;({{$hs->created_at->diffForHumans()}})</td>
            <td>{{$hs->order->tracking_code}}</td>
            <td>{{$hs->invoice_number}}</td>
            <td>₦ {{number_format($hs->amount,2)}}</td>
            <td>{{$hs->status}}</td>
            <td>{{$hs->order->status}}</td>
            </tr>
          @endforeach 
       
          {{-- <tr>{{$hist->links()}}</tr> --}}
      </tbody>
  </table>
        
    @endif
    
  @endif


  @if (isset($cshOnDel))
      
      <h4 class="text-center" style="margin-top:30px"><i><u><strong>Cash On Delivery Orders</strong></u></i></h4>

        @if($cshOnDel->isEmpty())
          <p class="ord text-center">{{'No available record found'}}</p>
        @else
      <table class="table table-striped wow fadeInUp" style="margin-bottom:50px">
        <thead>
          <tr>
            <th>SN</th>
            <th>Date</th>
            <th>Tracking Code</th>
            <th>Invoice Number</th>
            <th>Invoice Amount</th>
            <th>Payment Status</th>
            <th>Delivery Status</th>
          </tr>
        </thead>
        <tbody>
          
            @foreach ($cshOnDel as $csh)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$csh->created_at}}&nbsp; &nbsp;({{$csh->created_at->diffForHumans()}})</td>
                <td>{{$csh->cash_on_delivery_order->tracking_code}}</td>
                <td>{{$csh->invoice_number}}</td>
                <td>{{$csh->amount}}</td>
                <td>{{$csh->status}}</td>
                <td>{{$csh->cash_on_delivery_order->status}}</td>
              </tr> 
            @endforeach
        </tbody>
    </table>
      @endif
  @endif
  

@endsection