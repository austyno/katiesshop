@extends('shop.layout.main')

@section('content')
  <div class="breadcrumb">
      <div class="container">
        <div class="breadcrumb-inner">
          <ul class="list-inline list-unstyled">
          <li><a href="{{ route('shop.home')}}">Home</a></li>
            <li class='active'>Shopping Cart</li>
          </ul>
        </div><!-- /.breadcrumb-inner -->
      </div><!-- /.container -->
    </div>

    <div class="body-content outer-top-xs">
        <div class="container">
          <div class="row ">
            <div class="shopping-cart">
              <div class="shopping-cart-table ">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th class="cart-romove item">Remove</th>
                <th class="cart-description item">Image</th>
                <th class="cart-product-name item">Name</th>
                <th class="cart-product-name item">Price</th>
                <th class="cart-edit item"></th>
                <th class="cart-qty item">Quantity</th>
                <th class="cart-sub-total item">Subtotal</th>
              </tr>
            </thead><!-- /thead -->
            <tfoot>
              <tr>
                <td colspan="7">
                  <div class="shopping-cart-btn">
                    <span class="">
                      <a href="{{ route('shop.home')}}" class="btn btn-upper btn-primary outer-left-xs">Continue Shopping</a>
                    </span>
                  </div><!-- /.shopping-cart-btn -->
                </td>
              </tr>
            </tfoot>
            <tbody>
              @if(Cart::isEmpty())
              <tr class="text-center"><td colspan="100%"><p style="font-size:20px"><i>you have no items in your cart</i></p></td></tr>
              @else
              
              @foreach($content as $item)
                <tr>
                <td class="romove-item"><a href="{{ route('cart.remove',['id' => $item->id])}}" title="cancel" class="icon"><i class="fa fa-trash-o"></i></a></td>
                  <td class="cart-image">
                  <a class="entry-thumbnail" href="{{route('product.detail',['id'=>$item->attributes['slug']])}}"><img src="{{asset($item->attributes['image'])}}" alt=""></a>
                  </td>
                  <td class="cart-product-name-info">
                    <h4 class='cart-product-description'><a href="detail.html">{{$item->name}}</a></h4>
                    <div class="row">
                      <div class="col-sm-4">
                      @if($item->attributes['discount'])
                      <div class="rating rateit-small"><em style="color:green">Discount {{$item->attributes['discount']}}%</em></div>
                      @endif
                      </div>
                    </div><!-- /.row -->
                    <div class="cart-product-info">
                        {{-- <span class="product-color">COLOR:<span>Blue</span></span> --}}
                    </div>
                  </td>
                <td class="cart-product-name-info" style="width:auto">
                  <span class="cart-sub-total-price" style="font-weight:700px;font-size:16px"> ₦{{number_format($item->getPriceWithConditions(),2)}} </span>
                  
                </td>
                  <td class="cart-product-edit">
                      @if($item->attributes['discount'])
                        <span style="color:grey"><strike>₦{{$item->formated_price}}</strike></span>
                      @endif
                  </td>
                  <td class="cart-product-quantity">
                    <div class="quant-input">
                      <div class="arrows">
                      <div class="arrow plus gradient"><a href="{{ route('cart.incr',['id' => $item->id])}}" ><span class="ir"><i class="icon fa fa-sort-asc"></i></span></a></div>
                        <div class="arrow minus gradient"><a href="{{ route('cart.decr',['id' => $item->id])}}"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></a></div>
                      </div>
                      <input type="text" value="{{$item->quantity}}">
                    </div>
                  </td>
                  <td class="cart-product-sub-total"><span class="cart-sub-total-price">₦{{number_format($item->getPriceSumWithConditions(),2)}}</span></td>
                </tr>
              @endforeach
              @endif
            </tbody><!-- /tbody -->
          </table><!-- /table -->
        </div>
      </div><!-- /.shopping-cart-table -->				
      <div class="col-md-4 col-sm-12 estimate-ship-tax">
      </div><!-- /.estimate-ship-tax -->
      
      <div class="col-md-4 col-sm-12 estimate-ship-tax">
        <table class="table">
          <thead>
          </thead>
        </table><!-- /table -->
      </div><!-- /.estimate-ship-tax -->
      
      <div class="col-md-4 col-sm-12 cart-shopping-total">
        <table class="table">
          <thead>
            <tr>
              <th>
              <div class="cart-sub-total" style="text-align:right;">VAT &nbsp;<span class="inner-left-md" style="margin-right:20px">7.2 %</span></div>
                <div class="cart-sub-total">Subtotal<span class="inner-left-md">₦{{number_format(Cart::getSubTotal(),2)}}</span></div>
                <div class="cart-grand-total">Grand Total<span class="inner-left-md">₦{{number_format(Cart::getTotal(),2)}}</span></div>
              </th>
            </tr>
          </thead><!-- /thead -->
          <tbody>
              <tr>
                <td>
                  <div class="cart-checkout-btn pull-right">
                    <a href="{{ route('checkout')}}" type="submit" class="btn btn-primary">PROCCED TO CHEKOUT</a>
                    {{-- <span class="">Checkout with multiples address!</span> --}}
                  </div>
                </td>
              </tr>
          </tbody><!-- /tbody -->
        </table><!-- /table -->
      </div><!-- /.cart-shopping-total -->			
    </div><!-- /.shopping-cart -->
  </div> <!-- /.row -->
</div>
</div>
@endsection