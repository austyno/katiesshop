@extends('shop.layout.main')

@section('content')
<div class="breadcrumb">
    <div class="container">
      <div class="breadcrumb-inner">
        <ul class="list-inline list-unstyled">
        <li><a href="{{ route('shop.home')}}">Home</a></li>
          <li class='active'>Checkout</li>
        </ul>
      </div><!-- /.breadcrumb-inner -->
    </div><!-- /.container -->
  </div>

  <div class="body-content outer-top-xs">
      <div class="container">
        <div class="row ">
          <div class="shopping-cart">
            <div class="shopping-cart-table ">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              {{-- <th class="cart-romove item">Remove</th> --}}
              <th class="cart-description item">Image</th>
              <th class="cart-product-name item">Name</th>
              <th class="cart-product-name item">Price</th>
              <th class="cart-edit item"></th>
              {{-- <th class="cart-qty item">Quantity</th> --}}
              <th class="cart-sub-total item">Subtotal</th>
            </tr>
          </thead><!-- /thead -->
          <tfoot>
            <tr>
              <td colspan="7">
                <div class="shopping-cart-btn">
                  <span class="">
                    <a href="{{ route('shop.home')}}" class="btn btn-upper btn-primary outer-left-xs">Continue Shopping</a>
                    {{-- <a href="#" class="btn btn-upper btn-primary pull-right outer-right-xs">Update shopping cart</a> --}}
                  </span>
                </div><!-- /.shopping-cart-btn -->
              </td>
            </tr>
          </tfoot>
          <tbody>
            @foreach(Cart::getContent() as $item)
              <tr>
              {{-- <td class="romove-item"><a href="{{ route('cart.remove',['id' => $item->id])}}" title="cancel" class="icon"><i class="fa fa-trash-o"></i></a></td> --}}
                <td class="cart-image">
                <a class="entry-thumbnail" href="{{route('product.detail',['id'=>$item->attributes['slug']])}}"><img src="{{asset($item->attributes['image'])}}" alt=""></a>
                </td>
                <td class="cart-product-name-info">
                  <h4 class='cart-product-description'><a href="{{route('product.detail',['id'=>$item->attributes['slug']])}}">{{$item->name}}</a></h4>
                  <div class="row">
                    <div class="col-sm-4">
                    @if($item->attributes['discount'])
                    <div class="rating rateit-small"><em style="color:green">Discount {{$item->attributes['discount']}}%</em></div>
                    @endif
                    </div>
                  </div><!-- /.row -->
                  <div class="cart-product-info">
                      {{-- <span class="product-color">COLOR:<span>Blue</span></span> --}}
                  </div>
                </td>
              <td class="cart-product-name-info" style="width:auto">
                <span class="cart-sub-total-price" style="font-weight:700px;font-size:16px"> ₦{{number_format($item->getPriceWithConditions())}} </span>
              </td>
                <td class="cart-product-edit">
                    @if($item->attributes['discount'])
                      <span style="color:grey"><strike>₦ {{number_format($item->price,2)}}</strike></span>
                    @endif
                </td>
                <td class="cart-product-sub-total"><span class="cart-sub-total-price">₦{{number_format($item->getPriceSumWithConditions(),2)}}</span></td>
              </tr>
            @endforeach
          </tbody><!-- /tbody -->
        </table><!-- /table -->
      </div>
    </div><!-- /.shopping-cart-table -->				
    <div class="col-md-4 col-sm-12 estimate-ship-tax">
    </div><!-- /.estimate-ship-tax -->
    
    <div class="col-md-4 col-sm-12 estimate-ship-tax">
      <table class="table">
        <thead>
        </thead>
      </table><!-- /table -->
    </div><!-- /.estimate-ship-tax -->
    
    <div class="col-md-4 col-sm-12 cart-shopping-total">
      <table class="table">
        <thead>
          <tr>
            <th>
              <div class="cart-sub-total" style="text-align:right;">VAT &nbsp;<span class="inner-left-md" style="margin-right:38px">7.2%</span></div>
              <div class="cart-sub-total">Subtotal<span class="inner-left-md">₦{{number_format(Cart::getSubTotal(),2)}}</span></div>
              <div class="cart-grand-total">Grand Total<span class="inner-left-md">₦{{number_format(Cart::getTotal(),2)}}</span></div>
            </th>
          </tr>
        </thead><!-- /thead -->
        <tbody>
            <tr>
              <td>
                  <div class="cart-checkout-btn pull-left">
                      <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                        @csrf
                          <div class="row" style="margin-bottom:40px;">
                              <input type="hidden" name="email" value="just@gmail.com"> {{-- required --}}
                              <input type="hidden" name="orderID" value="345">                       
                              <input type="hidden" name="amount" value="{{Cart::getTotal()*100}}"> {{-- required in kobo --}}
                              <input type="hidden" name="quantity" value="{{Cart::getTotalQuantity()}}">
                              <input type="hidden" name="metadata" value="{{ json_encode($array = ['id' => Auth::user()->id]) }}" >
                              <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                              <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}                  
                              <button class="btn btn-success" type="submit" value="Pay Now!"><i class="fa fa-plus-circle fa-lg"></i> Pay Now!</button>
                            </div>
                          </div>
                    </form>
                  </div>
                <div class="cart-checkout-btn pull-right" style="margin-left:10px">
                <a href="{{ route('cash.delivery') }}" class="btn btn-info"><i class="fa fa-plus-circle fa-lg"></i> Cash On Delivery</a>
                </div>
              </td>
            </tr>
        </tbody><!-- /tbody -->
      </table><!-- /table -->
    </div><!-- /.cart-shopping-total -->			
  </div><!-- /.shopping-cart -->
</div> <!-- /.row -->
</div>
</div>
@endsection