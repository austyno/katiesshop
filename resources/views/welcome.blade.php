@extends('shop.layout.main')

@section('content')

<div id="hero">
        <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
          <div class="item" style="background-image: url(shop/assets/images/sliders/child2.png);">
            <div class="container-fluid">
              <div class="caption bg-color vertical-center text-left">
                <div class="slider-header fadeInDown-1"></div>
                <div class="big-text fadeInDown-1" style="margin-left:70px">Children <span class="highlight"> Toys</span></div>
                <div class="excerpt fadeInDown-2 hidden-xs" style="margin-left:90px"> <span>Play time is learning time! Get quality toys here</span> </div>
                <div class="button-holder fadeInDown-3" style="margin-left:180px"> <a href="{{route('product.category',['cat'=>'category','name' => 'toys','id' => 4])}}" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a> </div>
              </div>
              <!-- /.caption --> 
            </div>
            <!-- /.container-fluid --> 
          </div>
          <!-- /.item -->
          
          <div class="item" style="background-image: url(shop/assets/images/sliders/bts2.png);">
            <div class="container-fluid">
              <div class="caption bg-color vertical-center text-left" style="margin-top:-40px">
                <div class="slider-header fadeInDown-1"></div>
                <div class="big-text fadeInDown-1" style="margin-left:40px;margin-top:20px">Back To <span class="highlight">School</span> </div>
                <div class="excerpt fadeInDown-2 hidden-xs" style="margin-left:70px"> 
                  <span>we have in stock all kinds of back to school items</span> 
                </div>
                <div class="button-holder fadeInDown-3" style="margin-left:170px"> <a href="{{route('product.category',['cat'=>'category','name' => 'B2S Plus','id' => 5])}}" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a> </div>
              </div>
              <!-- /.caption --> 
            </div>
            <!-- /.container-fluid --> 
          </div>
          <!-- /.item --> 

          <div class="item" style="background-image: url(shop/assets/images/sliders/preg2.jpg);" >
            <div class="container-fluid">
              <div class="caption bg-color vertical-center text-left">
                <div class="slider-header fadeInDown-1"></div>
                <div class="big-text fadeInDown-1" style="margin-left:-5px;font-size:40px;margin-top:7px">Expectant <span class="highlight">Mothers</span> </div>
                <div class="excerpt fadeInDown-2 hidden-xs text-center" style="margin-left:;"> 
                  <span>We have all that you will need under one roof</span> 
                </div>
                <div class="button-holder fadeInDown-3" style="margin-left:180px"> <a href="{{route('product.category',['cat'=>'category','name' => '4 Mums','id' => 8])}}" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a> </div>
              </div>
              <!-- /.caption --> 
            </div>
            <!-- /.container-fluid --> 
          </div>
          
        </div>
        <!-- /.owl-carousel --> 
      </div>

      <div class="info-boxes wow fadeInUp">
            <div class="info-boxes-inner">
              <div class="row">
                <div class="col-md-6 col-sm-4 col-lg-4">
                  <div class="info-box">
                    <div class="row">
                      <div class="col-xs-12">
                        <h4 class="info-box-heading green">Quality Guaranteed</h4>
                      </div>
                    </div>
                    <h6 class="text">30 Days Money Back Guarantee</h6>
                  </div>
                </div>
                <!-- .col -->
                
                <div class="hidden-md col-sm-4 col-lg-4">
                  <div class="info-box">
                    <div class="row">
                      <div class="col-xs-12">
                        <h4 class="info-box-heading green">Fast shipping</h4>
                      </div>
                    </div>
                    <h6 class="text"> Orders shipped ASAP</h6>
                  </div>
                </div>
                <!-- .col -->
                
                <div class="col-md-6 col-sm-4 col-lg-4">
                  <div class="info-box">
                    <div class="row">
                      <div class="col-xs-12">
                        <h4 class="info-box-heading green">Secure Payment</h4>
                      </div>
                    </div>
                    <h6 class="text">Best in industry Payment System </h6>
                  </div>
                </div>
                <!-- .col --> 
              </div>
              <!-- /.row --> 
            </div>
            <!-- /.info-boxes-inner --> 
            
        </div> 
        <!-- /.nav-tabs --> 
        
        <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar">
                <div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs animated" style="visibility: visible; animation-name: fadeInUp;">
                    <h3 class="section-title">hot deals</h3>
                  <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss">
                    @foreach($hotdeals as $h)
                    <div class="item">
                      <div class="products">
                        <div class="hot-deal-wrapper">
                          <div class="image"> 
                            <a href="{{ route('product.detail',['slug' => $h->slug])}}"><img src="{{ asset($h->image)}}" alt="" style="width:223px;height:223px"></a>
                          </div>
                            <div class="sale-offer-tag"><span>{{$h->discount}}%<br>off</span></div>
                        </div>
                        <!-- /.hot-deal-wrapper -->
                <div class="product-info text-left m-t-20">
                    <h3 class="name"><a href="{{ route('product.detail',['slug' => $h->slug])}}">{{$h->name}}</a></h3>
                        <div class="rating rateit-small"></div>
                    <div class="product-price">
                      <span class="price">₦ {{$h->discount_price}}</span> 
                      <span class="price-before-discount">₦ {{$h->formated_price}} </span> </div>
                    <!-- /.product-price --> 
                    
                </div>
                <!-- /.product-info -->
                        
                <div class="cart clearfix animate-effect">
                    <div class="action">
                    <div class="add-cart-button btn-group">
                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i class="fa fa-shopping-cart"></i> </button>
                        <a style="padding:0px;border:0px">
                          <form method="get" action="{{ route('cart.add',['id' => $h->id])}}">
                            @csrf
                            <input class="btn btn-primary cart-btn" type="submit" value="Add To Cart">
                          </form>
                        </a>  
                    </div>
                    </div>
                    <!-- /.action --> 
                </div>
                <!-- /.cart --> 
                </div>
            </div>
            @endforeach
            </div><!-- /.sidebar-widget -->
        </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">       
          <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
            <div class="more-info-tab clearfix ">
              <h3 class="new-product-title pull-left">New Products</h3>
            <ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
              <li class="active"><a data-transition-type="backSlide" href="#all" data-toggle="tab">All</a></li>
              <li><a data-transition-type="backSlide" href="#cloths" data-toggle="tab">Clothing</a></li>
              <li><a data-transition-type="backSlide" href="#toys" data-toggle="tab">Toys</a></li>
              <li><a data-transition-type="backSlide" href="#shoes" data-toggle="tab">Shoes</a></li>
            </ul>
        <!-- /.nav-tabs --> 
          </div>
          <div class="tab-content outer-top-xs">
            <div class="tab-pane in active" id="all">
              <div class="product-slider">
                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="3">

                 @foreach ($all as $a)
                  <div class="item item-carousel">
                      <div class="products">
                        <div class="product">
                          <div class="product-image">
                            <div class="image"> 
                              <a href="{{ route('product.detail',['slug' =>$a->slug])}}">
                                <img  src="{{$a->image}}" alt="" style="height:254px" ></a> 
                            </div>
                            <!-- /.image -->             
                              <div class="tag new">
                                <span>new</span>
                              </div>
                            </div>
                            <!-- /.product-image -->
                                
                            <div class="product-info text-left">
                              <h3 class="name"><a href="{{ route('product.detail',['slug' => $a->slug])}}">{{$a->name}}</a></h3>
                              <div class="rating rateit-small"></div>
                              <div class="description">{!! substr($a->description,0,150) !!}</div> 
                              <div class="product-price">  
                                  @if ($a->discount)
                                  <span class="price">₦ {{$a->discount_price}} </span> 
                                  <span class="price-before-discount">₦ {{$a->formated_price}}</span> 
                                  @else
                                  <span class="price">₦ {{$a->formated_price}}
                                  @endif 
                              </div>
                              <!-- /.product-price -->                  
                            </div>
        
                      <!-- /.product-info -->
                            <div class="cart clearfix animate-effect">
                              <div class="action">
                                <ul class="list-unstyled">
                                  <li class="add-cart-button btn-group">
                                    <a href="{{ route('cart.add',['id' => $a->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>
                                    {{-- <button class="btn btn-primary cart-btn" type="button">Add to cart</button> --}}
                                  </li>
                                  <li class="lnk wishlist"> <a data-toggle="tooltip" class="add-to-cart" href="{{ route('product.detail',    ['slug' =>$a->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                                  <li class="lnk">
                                    <a class="add-to-cart" href="{{$a->image}}" data-fancybox data-caption="{{$a->name}} &nbsp; Price ₦ {{$a->formated_price}} " title="Quick View" > <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                                  </li>
                                </ul>
                              </div>
        
                                <!-- /.action --> 
                            </div>
                    <!-- /.cart --> 
                        </div>
                    <!-- /.product --> 
        
                      </div>
                    <!-- /.products --> 
                  </div>
                @endforeach
   
            </div>
        <!-- /.home-owl-carousel --> 
        </div>
        <!-- /.product-slider --> 
      </div>
                    
        <div class="tab-pane" id="cloths">
          <div class="product-slider">
          <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
            @foreach ($newcloths as $cl)
              <div class="item item-carousel">
                  <div class="products">
                    <div class="product">
                      <div class="product-image">
                        <div class="image"> <a href="{{ route('product.detail',['slug' =>$cl->slug])}}"><img  src="{{ asset($cl->image)}}" alt=""></a> </div>
                        <!-- /.image -->
                        @if ($cl->stock < 5)
                        <div class="tag hot" style="line-height:20px;top:5%"><span>sold out</span></div>
                        @endif
                        
                      </div>
                      <!-- /.product-image -->
                      
                      <div class="product-info text-left">
                        <h3 class="name"><a href="{{ route('product.detail',['slug' =>$cl->slug])}}">{{$cl->name}}</a></h3>
                        <div class="rating rateit-small"></div>
                        <div class="description"><i>{!! substr($cl->description,0,150) !!}</i></div> 
                      <div class="product-price">
                        @if($cl->discount) 
                            <span class="price">₦ {{ $cl->discount_price }}</span> 
                            <span class="price-before-discount">{{$cl->formated_price}}</span> 
                        @else
                            <span class="price">{{$cl->formated_price}}</span> 
                        @endif
                        <!-- /.product-price --> 
                        </div>
                      </div>
                      <!-- /.product-info -->
                      <div class="cart clearfix animate-effect">
                        <div class="action">
                          <ul class="list-unstyled">
                            <li class="add-cart-button btn-group">
                                <a href="{{ route('cart.add',['id' => $cl->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>
                              {{-- <button class="btn btn-primary cart-btn" type="button">Add to cart</button> --}}
                            </li>
                            <li class="lnk wishlist"><a class="add-to-cart" href="{{ route('product.detail', ['slug' =>$cl->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                            <li class="lnk">
                                <a class="add-to-cart" href="{{$cl->image}}" data-fancybox data-caption="{{$cl->name}} &nbsp; Price ₦ {{$cl->formated_price}} " title="Quick View" > <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                            </li>
                          </ul>
                        </div>
                        <!-- /.action --> 
                      </div>
                      <!-- /.cart --> 
                    </div>
                    <!-- /.product --> 
                    
                  </div>
                  <!-- /.products --> 
              </div>
            @endforeach
                <!-- /.item -->
            </div>
            <!-- /.home-owl-carousel --> 
          </div>
          <!-- /.product-slider --> 
        </div>
        <!-- /.tab-pane -->
          
          <div class="tab-pane" id="toys">
            <div class="product-slider">
              <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                @foreach ($newToys as $t)
                  <div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="{{ route('product.detail',['slug' =>$t->slug])}}"><img  src="{{ asset($t->image)}}" alt=""></a> </div>
                          <!-- /.image -->
                          @if ($t->stock < 5)
                          <div class="tag hot" style="line-height:20px;top:5%"><span>sold out</span></div> 
                          @else
                             @if ($t->discount)
                             <div class="sale-offer-tag"><span>{{$t->discount}}%<br>off</span></div> 
                             @endif
                             <div class="tag new"><span>new</span></div>
                          @endif
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-left">
                            {{-- @php
                              $names = preg_split("/\s+/",$t->name);
                            @endphp --}}
                          <h3 class="name"><a href="{{ route('product.detail',['slug' =>$t->slug])}}">{{$t->name}}</a></h3>
                          <div class="rating rateit-small"></div>
                          <div class="description"><i>{!! substr($t->description,0,150) !!}</i></div> 
                        <div class="product-price">  
                          @if ($t->discount)
                            <span class="price">₦ {{ $t->discount_price }}</span>
                            <span class="price-before-discount">{{$t->formated_price}}</span>
                          @else
                            <span class="price">₦ {{ $t->formated_price }}</span>
                          @endif 
                        </div>
                          <!-- /.product-price --> 
                          
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                          <div class="action">
                            <ul class="list-unstyled">
                              <li class="add-cart-button btn-group">
                                  <a href="{{ route('cart.add',['id' => $t->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>
                              </li>
                              <li class="lnk wishlist"><a class="add-to-cart" href="{{ route('product.detail',['slug'=>$t->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                              <li class="lnk">
                                  <a class="add-to-cart" href="{{$t->image}}" data-fancybox data-caption="{{$t->name}} &nbsp; Price ₦ {{$t->formated_price}} " title="Quick View" > <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                              </li>
                            </ul>
                          </div>
                          <!-- /.action --> 
                        </div>
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
                  </div>
                @endforeach
                
                <!-- /.item -->
              </div>
              <!-- /.home-owl-carousel --> 
            </div>
            <!-- /.product-slider --> 
          </div>
          <!-- /.tab-pane -->
          
        <div class="tab-pane" id="shoes">
            <div class="product-slider">
              <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

                @foreach ($newShoes as $nw)
                  <div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image" > 
                            <a href="{{ route('product.detail',['slug' =>$nw->slug])}}">
                              <img src="{{ asset($nw->image) }}" alt="" style="height:148px;" ></a> 
                            </div>
                          <!-- /.image -->
                            @if ($nw->stock < 5)
                            <div class="tag hot" style="line-height:20px;top:5%"><span>sold out</span></div>
                            @endif
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-left">
                          <h3 class="name"><a href="{{ route('product.detail',['slug' =>$nw->slug])}}">{{$nw->name}}</a></h3>
                          <div class="rating rateit-small"></div>
                          <div class="description">{!! substr($nw->description,0,150) !!}</div> 
                          <div class="product-price"> 
                            <span class="price">₦ {{$nw->formated_price}}</span> 
                          </div>
                          <!-- /.product-price --> 
                          
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                          <div class="action">
                            <ul class="list-unstyled">
                              <li class="add-cart-button btn-group">
                                  <a href="{{ route('cart.add',['id' => $nw->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>

                              </li>
                              <li class="lnk wishlist"> 
                                <a class="add-to-cart" href="{{ route('product.detail',['slug' =>$nw->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> 
                              </li>
                              <li class="lnk">
                                <a class="add-to-cart" href="{{ asset($nw->image) }}" data-fancybox="" data-caption="{{$nw->name}} &nbsp;&nbsp; Price ₦ {{$nw->formated_price}}" title="Quick View"> <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                              </li>
                            </ul>
                          </div>
                          <!-- /.action --> 
                        </div>
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
                  </div>
                @endforeach
                
              </div>
              <!-- /.home-owl-carousel --> 
            </div>
            <!-- /.product-slider --> 
          </div>
          <!-- /.tab-pane --> 
          
        </div>
      <!-- /.tab-content --> 
    </div>
  </div>
</div>

<!--banner-->
<div class="wide-banners wow fadeInUp outer-bottom-xs">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <div class="wide-banner cnt-strip">
          <div class="image"> 
          <a href="{{ route('shop.search',['q' => 'shoes'])}}" ><img class="img-responsive" src="{{ asset('shop/assets/images/banners/webbanner2.png')}}" alt=""></a> 
          </div>
        </div>
        <!-- /.wide-banner --> 
      </div>
      <!-- /.col -->
      <div class="col-md-6 col-sm-6">
        <div class="wide-banner cnt-strip">
          <div class="image"> 
            <a href="{{route('product.category',['cat'=>'category','name' =>'Fashion','id' =>3])}}"><img class="img-responsive" src="{{ asset('shop/assets/images/banners/web4.png')}}" alt=""></a> 
          </div>
        </div>
        <!-- /.wide-banner --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
</div>
<section class="section featured-product wow fadeInUp">
  <h3 class="section-title">Featured products</h3>
  <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
    @foreach ($featured as $ft)
      <div class="item item-carousel">
        <div class="products">
          <div class="product">
            <div class="product-image">
              <div class="image"> <a href="{{ route('product.detail',['slug'=>$ft->slug])}}"><img  src="{{ asset($ft->image)}}" alt="" height="206px" width="206px"></a> </div>
              <!-- /.image -->
                @if ($ft->discount)
                  <div class="tag hot" style="line-height:20px;top:5%"><span>-{{$ft->discount}}%<br>off</span></div>
                @endif
                @if ($ft->stock < 5) 
                  <div class="tag hot" style="line-height:20px;top:5%"><span>sold out</span></div>
                @endif
              
            </div>
            <!-- /.product-image -->
            
            <div class="product-info text-left">
              <h3 class="name"><a href="{{ route('product.detail',['slug' =>$ft->slug])}}">{{$ft->name}}</a></h3>
              <div class="rating rateit-small"></div>
              <div class="description">{!! substr($ft->description,0,150) !!}</div>  
                @if ($ft->discount)
                  <div class="product-price"> 
                    <span class="price"> ₦ {{$ft->discount_price}} </span>
                  <span class="price-before-discount">{{$ft->formated_price}}</span>
                @else
                  <div class="product-price"> <span class="price"> ₦ {{$ft->formated_price}} </span> 
                @endif 
              </div>
              <!-- /.product-price --> 
              
            </div>
            <!-- /.product-info -->
            <div class="cart clearfix animate-effect">
              <div class="action">
                <ul class="list-unstyled">
                  <li class="add-cart-button btn-group">
                      <a href="{{ route('cart.add',['id' => $ft->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>

                  </li>
                  <li class="lnk wishlist"> <a class="add-to-cart" href="{{ route('product.detail',['slug' =>$ft->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                  <li class="lnk">
                      <a class="add-to-cart" href="{{ asset($ft->image) }}" data-fancybox="" data-caption="{{$ft->name}} &nbsp;&nbsp; Price ₦ {{$ft->formated_price}}" title="Quick View"> <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                  </li>
                </ul>
              </div>
              <!-- /.action --> 
            </div>
            <!-- /.cart --> 
          </div>
          <!-- /.product --> 
          
        </div>
        <!-- /.products --> 
      </div>
    @endforeach
  </div>
</section>


  <section class="section featured-product wow fadeInUp">
    <h3 class="section-title">For Mums</h3>
    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
      @foreach ($mums as $ft)
        <div class="item item-carousel">
          <div class="products">
            <div class="product">
              <div class="product-image">
                <div class="image"> <a href="{{ route('product.detail',['slug'=>$ft->slug])}}"><img  src="{{ asset($ft->image)}}" alt="" height="148px"></a> </div>
                <!-- /.image -->
                  @if ($ft->discount)
                    <div class="tag hot" style="line-height:20px;top:5%"><span>-{{$ft->discount}}%<br>off</span></div>
                  @endif
                  @if ($ft->stock < 5) 
                    <div class="tag hot" style="line-height:20px;top:5%"><span>sold out</span></div>
                  @endif
                
              </div>
              <!-- /.product-image -->
              
              <div class="product-info text-left">
                <h3 class="name"><a href="{{ route('product.detail',['slug' =>$ft->slug])}}">{{$ft->name}}</a></h3>
                <div class="rating rateit-small"></div>
                <div class="description">{!! substr($ft->description,0,150) !!}</div> 
                  @if ($ft->discount)
                    <div class="product-price"> 
                      <span class="price"> ₦ {{$ft->discount_price}} </span>
                    <span class="price-before-discount">{{$ft->formated_price}}</span>
                  @else
                    <div class="product-price"> <span class="price"> ₦ {{$ft->formated_price}} </span> 
                  @endif 
                </div>
                <!-- /.product-price --> 
                
              </div>
              <!-- /.product-info -->
              <div class="cart clearfix animate-effect">
                <div class="action">
                  <ul class="list-unstyled">
                    <li class="add-cart-button btn-group">
                        <a href="{{ route('cart.add',['id' => $ft->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>

                    </li>
                    <li class="lnk wishlist"> <a class="add-to-cart" href="{{ route('product.detail',['slug' =>$ft->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                    <li class="lnk">
                        <a class="add-to-cart" href="{{ asset($ft->image) }}" data-fancybox="" data-caption="{{$ft->name}} &nbsp;&nbsp; Price ₦ {{$ft->formated_price}}" title="Quick View"> <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                    </li>
                  </ul>
                </div>
                <!-- /.action --> 
              </div>
              <!-- /.cart --> 
            </div>
            <!-- /.product --> 
            
          </div>
          <!-- /.products --> 
        </div>
      @endforeach
    </div>
  </section>

<div class="best-deal wow fadeInUp outer-bottom-xs">
    <h3 class="section-title">Best seller</h3>
    <div class="sidebar-widget-body outer-top-xs">
      <div class="owl-carousel best-seller custom-carousel owl-theme outer-top-xs">
          @foreach ($bestseller as $bs)
          <div class="item">
              <div class="products best-product">
                  
                <div class="product">
                    <div class="product-micro">  
                      <div class="row product-micro-row" style="float:inline-start"> 
                        <div class="col col-xs-5">
                          <div class="product-image">
                            <div class="image"> <a href="{{ route('product.detail',['slug' =>$bs->slug])}}"> <img src="{{ asset($bs->image)}}" alt=""> </a> </div>
                            <!-- /.image --> 
                            
                          </div>
                          <!-- /.product-image --> 
                        </div>
                       
                        <!-- /.col -->
                        <div class="col2 col-xs-7">
                          <div class="product-info">
                            <h3 class="name"><a href="{{ route('product.detail',['slug' =>$bs->slug])}}">{{$bs->name}}</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="product-price"> <span class="price"> ₦ {{$bs->formated_price}} </span> </div>
                            <!-- /.product-price --> 
                            
                          </div>
                        </div>
                        
                        <!-- /.col --> 
                      </div>
                      
                      <!-- /.product-micro-row --> 
                    </div>
                    <!-- /.product-micro --> 
                   
                  </div>
              </div>
            </div>
            @endforeach
      </div>
    </div>
</div>

<section>
    <div class="row">   
      <div class="" style="margin-left:12px;margin-bottom:10px;margin-top:10px;">
          <img src="{{asset('shop/assets/images/webbannercopy.png')}}">
          <h3 style="margin-top:-150px;margin-left:350px;position:absolute"><i>We offer world class quality products <br>for your kids at affordable prices<br> and mouthwatering discounts that<br> you can’t find elsewhere.</i></h3>
      </div>    
    </div>
    <div  style="position:clear-all"></div>
  </section>

  <section class="section wow fadeInUp new-arriavls">
      <h3 class="section-title">New Arrivals</h3>
      <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
        @foreach ($newArrivals as $na)
        <div class="item item-carousel">
            <div class="products">
              <div class="product">
                <div class="product-image">
                  <div class="image"> <a href="{{ route('product.detail',['slug' =>$na->slug])}}"><img  src="{{ asset($na->image)}}" alt="" height="206px"></a> </div>
                  <!-- /.image -->
                  @if ($na->discount)
                    <div class="tag new"><span>-{{$na->discount}}%</span></div>
                  @endif
                  
                </div>
                <!-- /.product-image -->
                
                <div class="product-info text-left">
                  <h3 class="name"><a href="{{ route('product.detail',['slug' =>$na->slug])}}">{{$na->name}}</a></h3>
                  <div class="rating rateit-small"></div>
                  <div class="description">{!! substr($na->description,0,150) !!}</div> 
                  <div class="product-price"> 
                    @if ($na->discount)
                      <span class="price">₦ {{$na->discount_price}}</span>
                      <span class="price-before-discount">{{$na->formated_price}}</span>
                    @else
                      <span class="price">₦ {{$na->formated_price}}</span>  
                    @endif
                  
                  </div>
                  <!-- /.product-price --> 
                  
                </div>
                <!-- /.product-info -->
                <div class="cart clearfix animate-effect">
                  <div class="action">
                    <ul class="list-unstyled">
                      <li class="add-cart-button btn-group">
                          <a href="{{ route('cart.add',['id' => $na->id])}}" data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart"> <i class="fa fa-shopping-cart"></i></a>
                        {{-- <button class="btn btn-primary cart-btn" type="button">Add to cart</button> --}}
                      </li>
                      <li class="lnk wishlist"> <a class="add-to-cart" href="{{ route('product.detail',['slug' =>$na->slug])}}" title="Wishlist"> <i class="icon fa fa-heart"></i> </a> </li>
                      <li class="lnk">
                          <a class="add-to-cart" href="{{ asset($na->image) }}" data-fancybox="" data-caption="{{$na->name}} &nbsp;&nbsp; Price <?php if($na->discount){ echo '₦'.$na->discount_price;}else{ echo '₦'.''.$na->formated_price;} ?> " title="Quick View"> <i class="fa fa-eye" aria-hidden="true"></i> </a> 
                      </li>
                    </ul>
                  </div>
                  <!-- /.action --> 
                </div>
                <!-- /.cart --> 
              </div>
              <!-- /.product --> 
              
            </div>
            <!-- /.products --> 
          </div>
        @endforeach
      </div>
  </section>
@endsection
