@extends('admin.layouts.main')

@section('content')

<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Brands <small></small></h2>
            <a href="{{ route('admin.category.brands.new') }}" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i></a>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th><i>Image</i></th>
                  <th><i> Name</i></th>
                  <th></th>
                  
                </tr>
              </thead>
              <tbody>
                  
                  @foreach ($brands as $br)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td><img src="{{ asset($br->image)}}" height="50px"></td>
                    <td><strong>{{ $br->name }}</strong></td>
                 </tr>
                  @endforeach
              </tbody>
            </table>

          </div>
        </div>
      </div>


@endsection