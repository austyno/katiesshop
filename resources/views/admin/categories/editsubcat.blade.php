@extends('admin.layouts.main')

@section('content')
<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edit Category: {{ $subcat->name }}</h2>
              
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('admin/inc/errors')
              <br>
            <form  class="form-horizontal form-label-left" method="post" action="{{ route('admin.category.updatesubcat',['id' => $subcat->id]) }}">
                    @csrf
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subcatname">SubCategory Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="subcatname" value="{{ $subcat->name }}" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat">Select Category<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="cat" class="form-control col-md-7 col-xs-12">
                            @foreach ($cat as $c)
                                @if ($c->id === $subcat->category->id)
                                  <option value="{{ $c->id }}" selected>{{$c->name}}</option>  
                                @else
                                  <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <a href="{{ route('admin.category.allsubcat') }}" class="btn btn-primary" type="button">Cancel</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>

@endsection