@extends('admin.layouts.main')

@section('content')
<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>New Brand</h2>
              
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('admin/inc/errors')
              <br>
            <form  class="form-horizontal form-label-left" method="post" action="{{ route('admin.category.brands.new') }}" enctype="multipart/form-data">
                    @csrf
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catName">Brand Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="name" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Images </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <img id="pre" src="#" class="image-responsive" height="100" width="150" style="display:none" data-toggle="tooltip" title="image preview" />
                      <input type="file" name="image" multiple accept="image/*" class="form-control col-md-7 col-xs-12" onchange="document.getElementById('pre').src = window.URL.createObjectURL(this.files[0]); document.getElementById('pre').style.display ='inline-block'">
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <a href="{{ route('admin.category.brands') }}" class="btn btn-primary" type="button">Cancel</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>

@endsection