@extends('admin.layouts.main')

@section('content')

<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Categories <small>All categories</small></h2>
            <a href="{{ route('admin.category.new') }}" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i></a>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Category Name</th>
                  <th></th>
                  
                </tr>
              </thead>
              <tbody>
                  <?php $count = 1; ?>
                  @foreach ($categories as $cat)
                  <tr>
                  <td>{{$count}}</td>
                    <td>{{ $cat->name }}</td>
                  <td><a href="{{ route('admin.category.edit',['id' => $cat->id]) }}" ><i class="fa fa-2x fa-edit"></i></a></td>
                 </tr>
                 <?php $count++; ?>
                  @endforeach
              </tbody>
            </table>

          </div>
        </div>
      </div>


@endsection