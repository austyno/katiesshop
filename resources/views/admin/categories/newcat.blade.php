@extends('admin.layouts.main')

@section('content')
<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>New Category</h2>
              
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('admin/inc/errors')
              <br>
            <form  class="form-horizontal form-label-left" method="post" action="{{ route('admin.category.submit') }}">
                    @csrf
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catName">Category Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="catName" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <a href="{{ route('admin.category.all') }}" class="btn btn-primary" type="button">Cancel</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>

@endsection