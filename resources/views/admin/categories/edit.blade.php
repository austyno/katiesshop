@extends('admin.layouts.main')

@section('content')
<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
            <h2>Edit Category: {{$cat->name}}</h2>
              
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('admin/inc/errors')
              <br>
            <form  class="form-horizontal form-label-left" method="post" action="{{ route('admin.category.update',['id' => $cat->id]) }}">
                    @csrf
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="catName">Category <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="catName" value="{{ $cat->name }}" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="{{ route('admin.category.all')}}" class="btn btn-primary" type="button">Cancel</a>
                    <button type="submit" class="btn btn-success">Update</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>

@endsection