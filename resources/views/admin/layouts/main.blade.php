<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Katie Shop Admin </title>
    <link href="{{ asset('lib/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet" >
    <link rel="icon" href="../../../logo.png" type="image/ico" />
    <!-- Bootstrap -->
    <link href="{{ asset('lib/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('lib/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('lib/iCheck/skins/flat/green.css')}}" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('lib/bootstrap-progressbar/css/bootstrap-progressbar.min.css') }}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset('lib/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('lib/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('lib/css/custom.min.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/css/toastr.min.css') }}" rel="stylesheet">

    <style>
      .detail{
        font-weight:bold;
        font-size:17px;
        text-align: center;
      }
      .detailsdiv{
        padding-left:30px;
        padding-right:30px;
        padding-bottom:10px;
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"> <span>Katty Shop Admin</span></a>
            </div>

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home </a>

                  </li>
                  <li><a><i class="fa fa-tags"></i> Category <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{ route('admin.category.all') }}"> Categories</a></li>
                    <li><a href="{{ route('admin.category.brands') }}">Brands</a></li>
                    <li><a href="{{ route('admin.category.allsubcat') }}">Sub Categories</a></li>
                    <li><a href="{{ route('admin.category.new') }}">New Category</a></li>
                    <li><a href="{{ route('admin.category.newsubcat') }}">New SubCategory</a></li>
                    <li><a href="{{ route('admin.category.newsubsubcat') }}">New SubSubCategory</a></li>
                    
                    </ul>
                  </li>
                  <li><a><i class="fa fa-reorder"></i> Products <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{ route('admin.products.all') }}">All Products</a></li>
                    <li><a href="{{ route('admin.products.new') }}">Add Products</a></li>
                    
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Sales <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{ route('admin.all-orders') }}">Orders</a></li>
                    <li><a href="{{ route('admin.customers')}}">All Customers</a></li>
                    <li><a href="{{ route('admin.invoices') }}">All Invoices</a></li>
                    <li><a href="{{ route('admin.unpaid') }}">Unpaid Invoices</a></li>
                    </ul>
                  </li>
                  
                  </li>
                  
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  @if(Auth::guard('admin')->user()->name)
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      <img src="images/img.jpg" alt="">{{ Auth::guard('admin')->user()->name }}
                      <span class=" fa fa-angle-down"></span>
                    </a>
                  @endif
                  
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li>
                    <a href="{{ route('admin.logout') }}" 
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out pull-right"></i> 
                          Log Out
                    </a>
                  </li>
                      <form id="logout-form" method="post" action="{{ route('admin.logout') }}" style="display:none">
                        {{ csrf_field() }}
                      </form>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        
        <div class="right_col" role="main">
            @yield('content')
        </div>
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Katie Shop Admin 
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('lib/jquery/dist/jquery.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('lib/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ asset('lib/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{ asset('lib/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="..lib/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="..lib/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('lib/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{ asset('lib/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="../lib/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../lib/Flot/jquery.flot.js"></script>
    <script src="../lib/Flot/jquery.flot.pie.js"></script>
    <script src="../lib/Flot/jquery.flot.time.js"></script>
    <script src="../lib/Flot/jquery.flot.stack.js"></script>
    <script src="../lib/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../lib/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../lib/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../lib/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../lib/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="{{ asset('lib/jqvmap/dist/jquery.vmap.js')}}"></script>
    <script src="{{ asset('lib/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{ asset('lib/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('lib/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('lib/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('lib/js/custom.min.js')}}"></script>

    <!-- datatable -->
    <script src="{{ asset('lib/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>    
    <script src="{{ asset('lib/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>     
    <script src="{{ asset('lib/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>    
    <script src="{{ asset('lib/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{ asset('lib/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>

    <!-- Toastr -->
  <script src=" {{ asset('lib/js/toastr.min.js')}}"></script>
  <script>

      toastr.options = {
      "positionClass": "toast-top-right"
    }
    
      @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}")
      @endif

      @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}")
      @endif

    </script>
    <script src="{{asset('lib/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins : 'advlist autolink link image lists print preview emoticons spellchecker',
            spellchecker_language: 'en',
            toolbar: 'emoticons',
            valid_elements: "a[href|target=_blank],strong/b,div[align],br,p"
        });
    
    </script>
    <script>
      $(document).ready(function(){
        $('[data-tool="tooltip"]').tooltip(); 
      });
    </script>

    <script>
      $(document).ready(function(){
        $(document).on('click','#view', function(e){

        e.preventDefault();

        var url = $(this).data('url');
        $('#modal-loader').show();

        $.ajax({
            url : url,
            type : 'GET',
            dataType : 'html'
        })
        .done(function(data){
            data = JSON.parse(data);
            //console.log(data);
            $('#productImage').html('<img src="/' + data.image+'" style="height:350px;width:400px" >');
            $('#productName').html(data.name);
            $('#price').html('₦ '+ data.price );
            $('#color').html(data.color);
            $('#size').html(data.size);
            $('#category').html(data.category['name']);
            if(data.brand !== null){
              $('#brand').html(data.brand['name']);
            }
            if(data.subcategory !== null){
              $('#subcategory').html(data.subcategory['name']);
            }
            if(data.subsubcategory !== null){
              $('#subsubcategory').html( data.subsubcategory['name']);
            }
            $('#productDesc').html(data.description);
            $('#modal-loader').hide();
        })
        .fail(function(){
            $('#dynamic').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
            $('#modal-loader').hide();
        });

    });

    
// get all subcat and subsub cat for this id
  $('#maincat').change(function(){
    var id = $(this).val();
    var cat = $(this).data('cat')
    var url = '/ajaxcat';
    var token = "{{csrf_token()}}"
    
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'html',
      data: {
        id: id,
        cat: cat,
        _token: token
      }
    })
    .done(function(data){
        data = JSON.parse(data);

            if(data.length > 0){
              var display ='';
            for(var i = 0; i < data.length; i++){
              display += '<option value="'+data[i].id+'">'+data[i].name+'</option>'
            }
            $('#subcat').html(display);
            }else{
              '<option>No category selected</option>'
            }
    })
    .fail({
      
    });
  });




//get all subsub cat for this id
  $('#subcat').change(function(){
    var id = $(this).val();
    var cat = $(this).data('cat')
    var url = '/ajaxsubcat';
    var token = "{{csrf_token()}}"
  //  console.log(url);
    console.log(id)

    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'html',
      data: {
        id: id,
        cat: cat,
        _token: token
      }
    })
    .done(function(data){
        data = JSON.parse(data);

       if(data.length > 0){
        var display ='';
        for(var i = 0; i < data.length; i++){
          display += '<option value="'+data[i].id+'">'+data[i].name+'</option>'
        }
        $('#subsubcat').html(display);
       }else{
        $('#subsubcat').html('<option value= 0 >no categories</option>');
       }

    })
    .fail({});
  });



   });

    </script>
	
  </body>
</html>

    
