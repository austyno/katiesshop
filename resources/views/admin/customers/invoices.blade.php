@extends('admin.layouts.main')

@section('content')
@if(isset($inv))
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>All Invoices <small></small></h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Customer Name</th>
              <th>Ivoice Num</th>
              <th>Invoice Amount</th>
              <th>Invoice status</th>
              <th>Date Created</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($inv as $p)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$p->user->name}}</td>
                  <td>{{$p->invoice_number}}</td>
                  <td>₦ {{number_format($p->amount,2)}}</td>
                  <td>{{$p->status}}</td>
                  <td>{{$p->created_at}}&nbsp; {{$p->created_at->diffForHumans()}}</td>
                </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
@else
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>All Unpadid Invoices <small></small></h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Customer Name</th>
              <th>Ivoice Num</th>
              <th>Invoice Amount</th>
              <th>Invoice status</th>
              <th>Date Created</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($unpaid as $p)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$p->user->name}}</td>
                  <td>{{$p->invoice_number}}</td>
                  <td>₦ {{number_format($p->amount,2)}}</td>
                  <td>{{$p->status}}</td>
                  <td>{{$p->created_at}}&nbsp; {{$p->created_at->diffForHumans()}}</td>
                </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endif
@endsection