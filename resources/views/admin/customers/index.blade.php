@extends('admin.layouts.main')

@section('content')
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>All Customers <small></small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>#</th>
            <th>Customer Name</th>
            <th>Email</th>
            <th>Phone Num</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($cus as $p)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$p->name}}</td>
                <td>{{$p->email}}</td>
                <td>{{$p->phone}}</td>
                <td>{{$p->address}}</td>
              </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection