@extends('admin.layouts.main')

@section('content')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>All Paid Orders <small></small></h2>
          <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Customer Name</th>
              <th>Order Amount</th>
              <th>Order Date</th>
              <th>Order status</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($paid as $p)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$p->user->name}}</td>
                  <td>₦ {{$p->total}}</td>
                  <td>{{$p->created_at}} &nbsp;{{$p->created_at->diffForHumans()}}</td>
                  <td>{{$p->status}}</td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
      <h2>Cash On Delivery Orders <small>Showing {{$cod->count()}} of {{$total->count()}} entries</small></h2>
          <div class="clearfix"></div> 
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Customer Name</th>
              <th>Order Amount</th>
              <th>Order Date</th>
              <th>Order status</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($cod as $p)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$p->user->name}}</td>
                  <td>₦ {{$p->total}}</td>
                  <td>{{$p->created_at}} &nbsp;{{$p->created_at->diffForHumans()}}</td>
                  <td>{{$p->status}}</td>
                </tr>
            @endforeach
          </tbody>

        </table>
        {{$cod->links()}}<br><small>Showing {{$cod->count()}} of {{$total->count()}} entries</small>
      </div>
    </div>
  </div>
@endsection