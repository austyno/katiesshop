@extends('admin.layouts.main')

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Order Items</h2>
          <a class=" btn btn-success pull-right" href="{{route('dashboard')}}">back</a>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Customer Detail <small></small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Full Name</th>
                        <th>Phone Num.</th>
                        <th>Email</th>
                        <th>Address</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                      <td>{{$ods[0]->user->name}}</td>
                      <td>{{$ods[0]->user->phone}}</td>
                      <td>{{$ods[0]->user->email}}</td>
                      <td>{{$ods[0]->user->address}}</td>
                      </tr> 
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                      <div class="x_title">
                        <h2>Order Items <small></small></h2>
                          
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <table id="datatable-responsive" class="table table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Product Name</th>
                                <th>Qty</th>
                                <th>Date Created</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($ods as $item)
                                <tr>
                                <td>{{$loop->iteration}}</td>
                                <td><img src="{{asset($item->product->image)}}" height="80px" width="80px" /></td>
                                <td>{{$item->product->name}}</td>
                                <td>{{$item->qty}}</td>
                                <td>{{$item->created_at}}&nbsp;&nbsp; {{$item->created_at->diffForHumans()}}</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                      </div>
                  </div>         
              </div>
            
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection