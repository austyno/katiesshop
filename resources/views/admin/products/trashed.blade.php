@extends('admin.layouts.main')

@section('content')
<div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Trash <small> </small> </h3>
      </div>      
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
              <a href="{{ route('admin.products.all')}}" class="btn btn-success btn-xs pull-right">Products</a>
            <h2>Trashed Products <small></small></h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">
                @if ($products->count() > 0)
                @foreach ($products as $p)
                <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                          <img style="width: 100%; display: block;" src="{{ asset($p->image) }}" alt="image">
                            <div class="mask">
                              <p>{{ $p->name}}</p>
                              <div class="tools tools-bottom">
                                <a href="{{ route('admin.products.restore',['id'=>$p->id]) }}" data-toggle="tooltip" data-placement="top" title="Restore"><i class="fa fa-mail-reply"></i></a>
                                <a href="{{ route('admin.products.destroy',['id' => $p->id]) }}" data-toggle="tooltip" data-placement="top" title="Destroy"><i class="fa fa-trash" style="color:red"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption" style="padding:0px">
                            <p><strong>Price: {{ $p->price }}</strong></p>
                            <p><strong>Stock level: 
                                    @if ($p->stock <= 5) 
                                        <span style="color:red">{{$p->stock}}</span>
                                    @else  
                                        <span style="color:green">{{$p->stock}}</span>                             
                                    @endif
                            </strong></p>
                            <p><strong>Added: {{$p->created_at->diffForHumans()}}</strong></p>
                          </div>
                        </div>
                      </div>
                    
                @endforeach
                    
                @else
                   <p class="text-center" style="font-weight:bold;font-size:18px">Trash is empty</p> 
                @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

{{-- modal --}}
  <div class="modal fade" id="viewProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
       <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 id="productName" class="modal-title text-center"></h4>
             <div class="text-center" id="productImage"></div>
            </div>
              <div class="modal-body">

                <div id="modal-loader" style="display: none; text-align: center;">
                    <img src="{{asset('images/ajax-loader.gif')}}">
                </div>
                  <div class="container" style="">
                    <div id="price" class="row detailsdiv" ></div>
                    <div id="category" class="row detailsdiv"></div>
                    <div id="brand" class="row detailsdiv"></div>
                    <div id="color" class="row detailsdiv"></div>
                    <div id="size" class="row detailsdiv"></div>
                    <div id="productDesc" class="row detailsdiv" style="height:auto"></div>
                  </div>
              </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 
              </div>
           </div><!-- /.modal-content -->
    </div><!-- /.modal -->
  </div>
@endsection