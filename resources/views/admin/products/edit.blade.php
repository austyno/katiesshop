@extends('admin.layouts.main')

@section('content')
<div class="">
    <div class="page-title">
      <div class="title_left">
        <h4>Edit:&nbsp; <u><em>{{$product->name}}</em></u> <small> </small> </h4>
      </div> 
      
        <a href="{{ route('admin.products.all')}}" class="btn btn-warning pull-right">Cancel</a>
    
    </div>

    <div class="clearfix"></div>
<hr>
<div class="col-md-8 center-margin">
        @include('admin/inc/errors')
    <form class="form-horizontal form-label-left" method="post" action="{{ route('admin.products.update',['id' => $product->id])}}" enctype="multipart/form-data">
            @csrf
        <div class="form-group">
            <label>Product Name</label><br>
            <div class="col-md-8 inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="fa fa-shield"></i></span><input name="name" value="{{ $product->name }}" class="form-control" type="text" required></div>
            </div>
        </div>
        <div class="form-group">
            <label>Product Price</label><br>
            <div class="col-md-8 inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="fa fa-money"></i></span><input name="price" value="{{ $product->price}}" class="form-control" required type="number"></div>
            </div>
        </div>

        <div class="form-group">
            <label>Product discount</label><br>
            <div class="col-md-8 inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="fa fa-money"></i></span><input name="discount" value="{{ $product->discount}}" class="form-control" type="number"></div>
            </div>
        </div>

        <div class="form-group">
            <label>Product Color</label><br>
            <div class="col-md-8 inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="fa fa-money"></i></span><input name="color" value="{{ $product->color}}" class="form-control" type="text"></div>
            </div>
        </div>

        <div class="form-group">
            <label>Product Color</label><br>
            <div class="col-md-8 inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="fa fa-money"></i></span><input name="color" value="{{ $product->size}}" class="form-control" type="text"></div>
            </div>
        </div>
        
        <div class="form-group">
            <label>Stock level</label><br>
            <div class="col-md-8 inputGroupContainer">
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-calculator"></i></span><input name="stock" value="{{ $product->stock}}" class="form-control" type="number" required></div>
            </div>
        </div>
        <div class="form-group">
            <label>Product Image</label><br>
            <div class="col-md-8 inputGroupContainer">
                <label for="image">
                    @if ($product->image === '')
                        <img src="#" style="cursor:pointer" height="100" width="150" data-toggle="tooltip" title="Click on the image to change it"/>
                    @else     
                        <img src="{{ asset($product->image)}}" class="image-responsive" style="cursor:pointer" height="100" width="150" data-toggle="tooltip" title="Click on the image to change it"/>
                    @endif
                </label>
                <img id="pre" src="#" class="image-responsive" height="100" width="150" style="display:none" data-toggle="tooltip" title="image preview" />
            <input type="file" id="image"  name="image[]" multiple class="form-control" accept="image/*" style="display:none" onchange="document.getElementById('pre').src = window.URL.createObjectURL(this.files[0]); document.getElementById('pre').style.display ='inline-block'">
            </div>
        </div><br>
            <div class="form-group">
                <label> Category:</label><br>
                <div class="col-md-8 inputGroupContainer">
                    <select name="editcat" id="editcat" data-cat="maincat" >
                        @foreach ($cat as $c)
                            <option value="{{ $c->id }}" @if($c->id === $product->category_id) selected @endif>{{ $c->name }}</option> 
                        @endforeach
                    </select>
                </div>
            </div><br>
            <div class="form-group">
                <label>SubCategory:</label><br>
                <div class="col-md-8 inputGroupContainer">
                    <select name="editsubcat" id="editsubcat" data-cat="subcat">
                        <option value="{{$product->subcategory['id']}}">{{$product->subcategory['name']}}</option>
                    </select>
                </div>
            </div><br>

            <div class="form-group">
                <label> Sub SubCategory:</label><br>
                <div class="col-md-8 inputGroupContainer">
                    <select name="editsubsubcat" id="editsubsubcat">
                        <option value="{{$product->subsubcategory['id']}}">{{$product->subsubcategory['name']}}</option>
                    </select>
                </div>
            </div><br>

            <div class="form-group">
                <label> Brand:</label><br>
                <div class="col-md-8 inputGroupContainer">
                    <select name="brand">
                        @if ($product->brand['id'])
                        <option value="{{$product->brand['id']}}">{{$product->brand['name']}}</option> 
                        @else
                            <option value=0 >Select Brand</option>
                        @foreach ($brands as $b)
                            <option value="{{ $b->id }}">{{ $b->name }}</option>   
                        @endforeach 
                        @endif
                        
                    </select>
                </div>
            </div><br>
        <div class="form-group">
            <label>Description</label><br>
                <div class="col-md-8 inputGroupContainer">
                    <textarea name="desc" class="form-control">{{$product->description}}</textarea>
                </div>
            </div>      
        
        <div class="form-group">
            <button type="submit" class="btn btn-success">Update</button>
        <a href="{{ route('admin.products.all')}}" class="btn btn-warning">Cancel</a>
        </div>
    </form>
</div>
</div>
<script src="{{ asset('lib/jquery/dist/jquery.min.js')}}"></script>
<script>

$(document).ready(function(){

    // get all subcat and subsub cat for this id
    $('#editcat').change(function(){
    var id = $(this).val();
    var cat = $(this).data('cat')
    var url = '/ajaxcat';
    var token = "{{csrf_token()}}";
        
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
            id: id,
            cat: cat,
            _token: token
        }
    })
    .done(function(data){
        
        data = JSON.parse(data);
        
            if(data.length > 0){
                var display ='';
            for(var i = 0; i < data.length; i++){
                display += '<option value="'+data[i].id+'">'+data[i].name+'</option>'
            }
            $('#editsubcat').html(display);
            }else{
                '<option>No category selected</option>'
            }
    })
    .fail({});
    });



//get all subsub cat for this id
    $('#editsubcat').change(function(){
        var id = $(this).val();
        var cat = $(this).data('cat')
        var url = '/ajaxsubcat';
        var token = "{{csrf_token()}}"
    
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
            id: id,
            cat: cat,
            _token: token
        }
    })
    .done(function(data){
        data = JSON.parse(data);
        console.log(data);
        if(data.length > 0){
            var display ='';
            for(var i = 0; i < data.length; i++){
                display += '<option value="'+data[i].id+'">'+data[i].name+'</option>'
            }
                $('#editsubsubcat').html(display);
            }else{
                $('#editsubsubcat').html('');
            }
    })
        .fail({});
    });
});
</script>
@endsection
