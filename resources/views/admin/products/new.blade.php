@extends('admin.layouts.main')

@section('content')
<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>New Product</h2>
              
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @include('admin/inc/errors')
              <br>
              <form class="form-horizontal form-label-left" method="post" action="{{ route('admin.products.save')}}" enctype="multipart/form-data">
                    @csrf
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Name 
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product price </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" name="price" value="{{ old('price') }}" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Quantity </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" name="stock" value="{{ old('stock') }}" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Discount</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" name="discount" value="{{ old('discount') }}" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Color</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="color" value="{{ old('color') }}" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Size</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="size" value="{{ old('size') }}" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>
                
                <div class="ln_solid"></div>
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Categories</label>
                <div class="row" style="margin-left:-25px">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group col-md-4">
                            {{-- <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product 2</label> --}}
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="maincat" id="maincat" data-cat="maincat" >
                                    <option>main category</option>
                                    @foreach ($cat as $c)
                                        <option value="{{ $c->id }}">{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                                {{-- <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product 3</label> --}}
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <select name="subcat" id="subcat" data-cat="subcat">
                                    <option value=0>sub category</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            {{-- <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product 4</label> --}}
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <select name="subsubcat" id="subsubcat">
                                    <option value= 0 >Sub of Sub category</option>
                                </select>
                            </div>
                        </div><br>

                        <div class="form-group col-md-4">
                            {{-- <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product 4</label> --}}
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <select name="brand" id="brand">
                                    <option value="0">Brand category</option>
                                    @foreach ($brands as $br)
                                      <option value="{{ $br->id }}">{{ $br->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><br><br><br><br>

                        <div class="form-group">
                          <div class="col-md-8 inputGroupContainer">  
                              <label class="inline" style="margin-right:57px">
                                  <input type="radio" value="newproduct" name="status">&nbsp; New Products
                              </label>&nbsp; 
                              <label class="inline" >
                                  <input type="radio" value="hotdeal" name="status">&nbsp; Hot Deals
                              </label>&nbsp;
                              <label class="inline" style="margin-right:30px">
                                <input type="radio" value="featured" name="status">&nbsp; Featured Products
                            </label>&nbsp;
                            <label class="inline">
                              <input type="radio" value="bestseller" name="status">&nbsp; Best Sellers
                          </label>&nbsp; 
                          <label class="inline" style="margin-right:67px">
                            <input type="radio" value="newArrival" name="status">&nbsp; New Arrivals
                        </label>&nbsp;
                        <label class="inline">
                          <input type="radio" value="4mums" name="status">&nbsp; For Mums
                      </label>&nbsp;        
                          </div>
                      </div>
                    </div>        
                </div>
                <div class="ln_solid"></div>

                <div class="form-group">
                    <i class="fa fa-info-circle" data-toggle="tooltip" title="Attach atleast 3 images showing different angles and views" style="margin-left:10px;font-size:20px"></i>
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Images </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <img id="pre" src="#" class="image-responsive" height="100" width="150" style="display:none" data-toggle="tooltip" title="image preview" />
                      <input type="file" name="images[]" multiple accept="image/*" class="form-control col-md-7 col-xs-12" onchange="document.getElementById('pre').src = window.URL.createObjectURL(this.files[0]); document.getElementById('pre').style.display ='inline-block'">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Description </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <textarea name="desc" class="form-control col-md-7 col-xs-12">{{ old('desc') }}</textarea>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <a href="{{ route('admin.products.all') }}" class="btn btn-primary" type="button">Cancel</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>

@endsection