@extends('admin.layouts.main')

@section('content')
<div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Products <small> </small> </h3>
      </div>      
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
              <a href="{{ route('admin.products.trashed')}}" class="btn btn-info btn-xs pull-right">View Trash</a>
              <a href="{{ route('admin.products.new')}}" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i></a>
          
            <h2> Products <small> Most recent are at the top </small></h2>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">
                @foreach ($products as $p)
                <div class="col-md-55">
                        <div class="thumbnail" style="height">
                          <div class="image view view-first" style="">
                          <img style="width: 100%; display: block;" src="{{ asset($p->image) }}" alt="image">
                            <div class="mask">
                              @php
                                  $names = preg_split("/\s+/",$p->name);
                              @endphp
                              <p  style="padding-bottom:5px">{{ $names[0]}} {{$names[1]}}</p>
                              <div class="tools tools-bottom" style="height:100px">
                                <a href="#" id="view" data-url="{{ route('admin.products.viewdetails',['id' => $p->id])}}" data-toggle="modal" data-target="#viewProduct" data-tool="tooltip" data-placement="top" title="View details"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('admin.products.edit',['id'=>$p->id]) }}" data-toggle="tooltip" data-placement="top" title="Edit product"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('admin.products.delete',['id' => $p->id]) }}" data-toggle="tooltip" data-placement="top" title="Trash product"><i class="fa fa-trash"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption" style="padding:0px">
                            <p><strong>Price: ₦{{$p->formated_price}}</strong></p>
                            <p><strong>Stock level: 
                                    @if ($p->stock <= 5) 
                                        <span style="color:red;">{{$p->stock}}</span>
                                    @else  
                                        <span style="color:green;">{{$p->stock}}</span>                             
                                    @endif
                            </strong></p>
                            <p><strong>Added: {{$p->created_at->diffForHumans()}}</strong></p>
                          </div>
                        </div>
                      </div>
                    
                @endforeach
            </div>
            {{$products->links()}}
          </div>
        </div>
      </div>
    </div>
  </div>

{{-- modal --}}
  <div class="modal fade" id="viewProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
       <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 id="productName" class="modal-title text-center" style="margin:30px"></h4>
             <div class="text-center" id="productImage"></div>
            </div>
              <div class="modal-body">

                <div id="modal-loader" style="display: none; text-align: center;">
                    <img src="{{asset('images/ajax-loader.gif')}}">
                </div>
                  <div class="container" style="font-size:16px">
                    <table>
                      <tr>
                        <td class="detailsdiv">Price:</td>
                        <td id="price"></td>
                      </tr>
                      <tr>
                        <td class="detailsdiv">Color:</td>
                        <td id="color"></td>
                      </tr>
                      <tr>
                        <td class="detailsdiv">Size:</td>
                        <td id="size"></td>
                      </tr>
                      <tr>
                        <td class="detailsdiv">Category:</td>
                        <td id="category"></td>
                      </tr>
                      <tr>
                        <td class="detailsdiv">subcategory:</td>
                        <td id="subcategory"></td>
                      </tr>
                      <tr>
                        <td class="detailsdiv">sub subcategory:</td>
                        <td id="subsubcategory"></td>
                      </tr>
                      <tr>
                        <td class="detailsdiv">brand:</td>
                        <td id="brand"></td>
                      </tr>
                      <tr>
                        <td class="detailsdiv">productDesc:</td>
                        <td id="productDesc"></td>
                      </tr>
                    </table>
                  </div>
              </div>
              <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 
              </div>
           </div><!-- /.modal-content -->
    </div><!-- /.modal -->
  </div>
@endsection