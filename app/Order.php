<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
            'user_id',
            'invoice_number',
            'qty',
            'total',
            'status',
            'tracking_code'
    ];

    public function orderitems()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function paymentmethod()
    {
        return $this->belongsTo('App\PaymentMethod');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
