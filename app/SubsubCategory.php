<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubsubCategory extends Model
{
    protected $fillable = [
        'name','subcategory_id'
    ];
    public function subcategory()
    {
        return $this->belonsTo('App\SubCategory');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
