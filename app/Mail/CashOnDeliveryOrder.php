<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CashOnDeliveryOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $cust;
    //public $cart;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cust)
    {
        
        $this->cust = $cust;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('shop.emails.cash-on-delivery');
    }
}
