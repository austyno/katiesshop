<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;
use App\Category;
use App\Product;
use App\Brand;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //hot deals
        $hotdeals = Product::where('status', 'hotdeal')->orderBy('created_at', 'desc')->take(4)->get();

        //menu
        $menu = Category::with('subsubcategories')->get();
        
        //brands
        $brands = Brand::all();
        
        View::share(['menu' => $menu,'hotdeals' => $hotdeals, 'brands' => $brands]);
    }
}
