<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'user_id','order_id','invoice_number','status','amount'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
