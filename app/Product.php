<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $dates =['deleted_at'];

    protected $fillable = [
        'name',
        'price',
        'description',
        'image',
        'slug',
        'status',
        'discount',
        'stock',
        'color',
        'size',
        'brand_id',
        'category_id',
        'subcategory_id',
        'subsubcategory_id'

    ];
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    
    public function thumbnails()
    {
        return $this->hasMany('App\Thumbnail');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\SubCategory');
    }

    public function subsubcategory()
    {
        return $this->belongsTo('App\SubsubCategory');
    }

    public function getFormatedPriceAttribute(){

        return number_format($this->price,2);

    }
    
    public function getDiscountPriceAttribute(){

        return number_format($this->price - (($this->discount/100) * $this->price),2);
        
    }
}
