<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id','invoice_id','amount'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }
}
