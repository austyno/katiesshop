<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = [
        'category_id','name'
    ];
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function subsubcategories()
    {
        return $this->hasMany('App\SubsubCategory');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
