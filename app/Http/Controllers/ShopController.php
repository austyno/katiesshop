<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use SocialAuth;
use Auth;
use App\Order;
use App\Brand;
use App\Invoice;
use App\Product;
use App\OrderItem;
use App\Category;
use App\SubCategory;
use App\SubsubCategory;
use App\CashOnDeliveryInvoice;

class ShopController extends Controller
{
    public function home()
    {

        
//hot deals
        $hotdeals = Product::where('status', 'hotdeal')->orderBy('created_at', 'desc')->take(3)->get();

        $all = Product::where('status', 'newproduct')->orderBy('created_at', 'desc')->take(6)->get();

  //new shoes for boys and girls
        $boysshoescat = Subsubcategory::where('name', 'FootWear For Boys')->get();
        $girlsshoescat = Subsubcategory::where('name', 'FootWear For Girls')->get();
        $boyssheoescatid = $boysshoescat[0]->id;
        $girlsshoescatid = $girlsshoescat[0]->id;
        $prdtboys = Product::where('status', 'newproduct')->where('subsubcategory_id', $boyssheoescatid)->orderBy('created_at', 'desc')->take(3)->get();
        $prdtgirls = Product::where('status', 'newproduct')->where('subsubcategory_id', $girlsshoescatid)->orderBy('created_at', 'desc')->take(3)->get();
        $newShoes = collect([$prdtboys,$prdtgirls])->collapse();

// //new cloths
        $boysClothsCat = SubCategory::where('name', 'boys fashion')->get();
        $boysNewCloths = Product::where('status', 'newproduct')->where('subcategory_id', $boysClothsCat[0]->id)->orderBy('created_at', 'desc')->take(3)->get();
        $girlsClothsCat = SubCategory::where('name', 'girls fashion')->get();
        $girlsNewCloths = Product::where('status', 'newproduct')->where('subcategory_id', $girlsClothsCat[0]->id)->orderBy('created_at', 'desc')->take(3)->get();
        $newCloths = collect([$girlsNewCloths,$boysNewCloths])->collapse();

        $catToys = Category::where('name', 'toys')->get();
        $newToys = Product::where('status', 'newproduct')->where('category_id', $catToys[0]->id)->orderBy('created_at', 'desc')->take(5)->get();

  //featured
        $featured = Product::where('status', 'featured')->orderBy('created_at', 'desc')->get();

  //best seller
        $bestseller = Product::where('status', 'bestseller')->orderBy('created_at', 'desc')->get();

  //new arrivals
        $newArrivals = Product::where('status', 'newarrival')->orderBy('created_at', 'desc')->get();

  //4 mums
        $mums = Product::where('status', '4mums')->orderBy('created_at', 'desc')->get();

  
        return view('welcome')->with('hotdeals', $hotdeals)
                            ->with('newcloths', $newCloths)
                            ->with('newToys', $newToys)
                            ->with('newShoes', $newShoes)
                            ->with('featured', $featured)
                            ->with('bestseller', $bestseller)
                            ->with('newArrivals', $newArrivals)
                            ->with('mums',$mums)
                            ->with('all', $all);

    }

    public function detail(Request $request, $slug)
    {

        $prdt = Product::where('slug', $slug)->get();

      
      //get product in the same sub or subsub category as related products
        $catId = '';
        $cat ='';

        if ($prdt[0]->subsubcategory) {
            $catId = $prdt[0]->subsubcategory_id;
            $cat = 'subsubcategory_id';
        } else {
            $catId = $prdt[0]->subcategory_id;
            $cat = 'subcategory_id';
        }

        $relatedPrdt = Product::where($cat, $catId)->orderBy('created_at', 'desc')->take(7)->get();

        return view('shop.detail')->with('prdt', $prdt)
                                ->with('relatedPrdt', $relatedPrdt);
    }

    public function category(Request $request, $cat, $name, $id)
    {
        $category = $cat.'_id';


        $catproducts = Product::where($category, $id)->orderBy('created_at', 'desc')->paginate(16);


        return view('shop.category')->with('catproducts', $catproducts)
                                    ->with('cat', $cat)
                                    ->with('catName', $name);
    }

    public function brand($name)
    {

        $brandName = $name;
      
         $brd = Brand::where('name', $name)->get();

         $brandPrdt = Product::where('brand_id', $brd[0]->id)->paginate(16);

        return view('shop.brand')->with('brandPrdt', $brandPrdt)
                              ->with('brandName', $brandName);
    }

    public function signup()
    {
        return view('shop.signup');
    }

    public function track_orders()
    {

        return view('shop.track-order');
    }

    public function track(Request $request)
    {
     // tracking code
      $trackingCode = $request->code;
      $ord;

      // get order using invoice number
     $ord = Order::with('orderItems')->where('tracking_code',$trackingCode)->get();

     // if code was not found check cash on delivery
     if($ord->isEmpty()){

        $ord = CashOnDeliveryOrder::with('orderitems')->where('tracking_code',$trackingCode)->get();

      
     }else{
        Session::flash('info','the tracking code was not found in our records');

        return redirect()->back();
     }

      return view('shop.track-order')->with('ord',$ord);
                                    
    }

    public function search(Request $request)
    {
        $q = $request->q;
        $rslt = Product::where('name','LIKE','%'.$q.'%')->paginate(16);

        if($rslt->isEmpty()){
            Session::flash('info','Your search term '. $q .' did not return any products. Please try again');
            return back();
        }

        return view('shop.search')->with('result',$rslt)
                                ->with('q',$q);
    }

    public function history()
    {
      return view('shop.history');  
    }

    public function show_history(Request $request)
    {
        
         $hist = Invoice::with(['user','order'])->where('user_id',Auth::user()->id)->whereBetween('created_at', [$request->from, $request->ending])->get();
        
        $cshOnDel = CashOnDeliveryInvoice::with(['user','cash_on_delivery_order'])->where('user_id',Auth::user()->id)->whereBetween('created_at', [$request->from, $request->ending])->get();
            
        return view('shop.history')->with('hist', $hist)
                                    ->with('cshOnDel',$cshOnDel);
    }

    // public function auth($provider)
    // {
    //     return SocialAuth::authorize($provider);
    // }

    // public function auth_callback($provider)
    // {

    //     SocialAuth::login($provider, function($user, $details){
    //         $user->name = $details->full_name;
    //         $user->email = $details->email;
    //         $user->save();
    //         //dd($details);
    //     });

    //     return redirect('shop.home');
    // }
}
