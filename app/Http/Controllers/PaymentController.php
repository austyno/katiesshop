<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Paystack;
use Auth;
use Cart;
use Mail;
use Session;
use GuzzleHttp\Client;
use App\Product;
use App\Invoice;
use App\Order;
use App\OrderItem;
use App\Payment;
use App\CashOnDeliveryOrder;
use App\CashOnDeliveryOrderItem;
use App\CashOnDeliveryInvoice;

class PaymentController extends Controller
{
     /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    protected  $letrs = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
    protected $api_token = 'ACQukr6mZfIrL6Y8gWxCk7GLtEMjiAlqjCoYaHl9FRz0BrRrQ39G29mLccAs';
    
    private function invoiceNumber()
    {
        $l = str_shuffle($this->letrs);
        $inv = substr(mt_rand(), 0, 4).substr($l, 0, 2);

        return $inv;
    }

    private function trackingCode()
    {
        $t = str_shuffle($this->letrs);
        $code = substr(mt_rand(), 0, 8).substr($t, 0, 4);

        return 'KK'.$code;
    }

    private function sms($phoneNumber,$message)
    {
        $from = 'katykiddies';
        $to = $phoneNumber;
        $token = $this->api_token;
        $url = 'https://www.bulksmsnigeria.com/api/v1/sms/create?';

        $client = new Client();
        $response = $client->post($url,[
            'verify' => false,
            'form_params' => [
                'api_token' => $token,
                'from'  => $from,
                'to'    => $phoneNumber,
                'body'  => $message
            ]
        ]);
        $response = json_decode($response->getBody(), true);
    }

    public function redirectToGateway()
    {
        if (Auth::check()) {
            return Paystack::getAuthorizationUrl()->redirectNow();
        } else {
            Session::flash('info', 'You need to sign in or create a new account so we can email you your order');

            return view('shop.signup');
        }
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        //verify pmt data
        if ($paymentDetails['message'] === 'Verification successful' && $paymentDetails['data']['status'] === 'success') {
            $invoiceNumber = $this->invoiceNumber();
            $trackingCode = $this->trackingCode();
            
            // create the order
            $order = Order::create([
                'user_id'           => $paymentDetails['data']['metadata']['id'],
                'invoice_number'    => $invoiceNumber,
                'qty'               => Cart::getTotalQuantity(),
                'total'             => Cart::getTotal(),
                'tracking_code'     => $trackingCode,
            ]);

            $invoice = Invoice::create([
                'user_id'           =>  Auth::user()->id,
                'order_id'          =>  $order->id,
                'invoice_number'    =>  $invoiceNumber,
                'amount'            =>  Cart::getTotal(),
                'status'            =>  'paid'
            ]);

           //store order items
            foreach (Cart::getContent() as $item) {
                OrderItem::create([
                    'user_id'       => Auth::user()->id,
                    'order_id'      => $order->id,
                    'product_id'    => $item->id,
                    'price'         => $item->getPriceWithConditions(),
                    'qty'           => $item->quantity
                ]);
            }

           //store payment info
            Payment::create([
                'user_id'  =>   Auth::user()->id,
                'invoice_id'    => $invoice->id,
                'amount'        =>  Cart::getTotal()
            ]);

            
            $maildata = array(
                        'total'          =>  Cart::getTotal(),
                        'invoice'        =>  $invoiceNumber,
                        'name'           =>  Auth::user()->name,
                        'tracking_code'     => $trackingCode
                    );

            $orderDetails = array(
                            'customer name'     =>  Auth::user()->name,
                            'customer phone'    =>  Auth::user()->phone,
                            'customer email'    =>  Auth::user()->email,
                            'invoice number'    =>  $invoiceNumber,
                            'total qty'         =>  Cart::getTotalQuantity(),
                            'amount paid'       =>  number_format(Cart::getTotal(), 2),
                            'transaction id'    => $paymentDetails['data']['reference']
            );
               //send mail to customer
                Mail::to(Auth::user()->email)->send(new \App\Mail\PurchsaeSuccessful($maildata));

               //send mail to admin
                Mail::to('admin@katties.com')->send(new \App\Mail\OrderPlaced($orderDetails));

               //update stock level
                foreach (Cart::getContent() as $item) {
                $pro = Product::find($item->id);

                $pro->update([
                    'stock' =>  $pro->stock - $item->quantity
                    ]);
                }

                //send sms to admin
                $this->sms('+2347063535121','You have a new order at kattykiddies.com from '. Auth::user()->phone. ' check your email for details');

               //clear cart
                Cart::clear();

                Session::flash('success', 'We appreciate your patronage. Thank you so much.\n We have sent you a mail with your invoice number');

                return redirect(route('shop.home'));
        } else {
            echo 'sorry we are unable to process you request at this time';
        }
    }


    public function cash_on_delivery()
    {
        $inv = $this->invoiceNumber();
        $trackingCode = $this->trackingCode();

        if (Auth::check()) {
            // create the order
            $cshOnDelOrder = CashOnDeliveryOrder::create([
                    'user_id'        => Auth::user()->id,
                    'invoice_number' => $inv,
                    'qty'            => Cart::getTotalQuantity(),
                    'total'          => Cart::getTotal(),
                    'tracking_code'  => $trackingCode
            ]);

            // save order items
            foreach (Cart::getContent() as $item) {
                CashOnDeliveryOrderItem::create([
                    'user_id'                     => Auth::user()->id,
                    'cash_on_delivery_order_id'   => $cshOnDelOrder->id,
                    'product_id'                  => $item->id,
                    'price'                       => $item->getPriceWithConditions(),
                    'qty'                         => $item->quantity
                ]);
            };

                // save invoice
                $invoice = CashOnDeliveryInvoice::create([
                    'user_id'                      =>  Auth::user()->id,
                    'cash_on_delivery_order_id'    =>  $cshOnDelOrder->id,
                    'invoice_number'               =>  $inv,
                    'amount'                       =>  Cart::getTotal()
                ]);

                $orderDetails = array(
                    'customer name'     =>  Auth::user()->name,
                    'customer phone'    =>  Auth::user()->phone,
                    'customer email'    =>  Auth::user()->email,
                    'invoice number'    =>  $inv,
                    'total qty'         =>  Cart::getTotalQuantity(),
                    'amount paid'       =>  'cash on delivery',
    
                );
                
                $cust = collect(array(
                    'tracking_code' => $trackingCode,
                    'invoice' => $inv
                ));
                    

                Mail::to(Auth::user()->email)->send(new \App\Mail\CashOnDeliveryOrder($cust));

                Mail::to('admin@katties.com')->send(new \App\Mail\OrderPlaced($orderDetails));

                //update stock level
                foreach (Cart::getContent() as $item) {
                    $pro = Product::find($item->id);

                    $pro->update([
                        'stock' =>  $pro->stock - $item->quantity
                    ]);
                }
                
                $this->sms('+2347063535121','You have a new order at kattykiddies.com from '. Auth::user()->phone. ' check your email for details');
                Cart::clear();

                Session::flash('success', 'We appreciate your patronage. Thank you so much.\n We have sent you an  email with your order details');

                return redirect(route('shop.home'));


        } else {

            Session::flash('info', 'You need to sign in or create a new account so we can email you your order');
            return view('shop.signup');
        }
        
    }
}
