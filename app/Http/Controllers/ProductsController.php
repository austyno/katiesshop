<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;

use App\Category;
use App\Product;
use App\Brand;
use App\Color;
use App\Size;
use App\User;
use App\Order;
use App\Invoice;
use App\Thumbnail;
use App\OrderItem;
use App\Type;
use App\Attribute;
use App\SubCategory;
use App\SubsubCategory;
use App\CashOnDeliveryOrder;
use App\CashOnDeliveryInvoice;
use App\CashOnDeliveryOrderItem;
use Image;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->paginate(15);

        return view('admin.products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.new')->with('cat', Category::all())
                                        ->with('brands', Brand::all())
                                        ->with('subcat', SubCategory::all())
                                        ->with('subsub', SubsubCategory::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'maincat' => 'required',
            'subcat' => 'required',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'images' => 'required|max:1024',
        ]);

        $exist = Product::where('name',$request->name)->get();
    
        if(!empty($exist)){
            Session::flash('warning', 'A product with similar name already exist');
            return redirect()->back();
        }

        
        $image = time().'_'.$request->images[0]->getClientOriginalName();
        
        //insert into products
        $new_product = Product::create([
            'name'              => $request->name,
            'description'       => $request->desc,
            'image'             => 'productimages/'.$image,
            'price'             => $request->price,
            'color'             => $request->color ? $request->color : null,
            'size'              => $request->size  ? $request->size : null,
            'status'            => $request->status ? $request->status :null,
            'slug'              =>  str_slug($request->name, '-'),
            'stock'             => $request->stock,
            'brand_id'          => $request->brand ? $request->brand : null,
            'category_id'       => $request->maincat,
            'subcategory_id'    => $request->subcat,
            'subsubcategory_id' => $request->subsubcat ? $request->subsubcat : 0,
            'discount'          => $request->discount ? $request->discount : 0
        ]);

        //images from form
        $images = $request->images;
        
        //loop thru all images from form
        foreach ($images as $img) {
            $imgName = time().'_'.$img->getClientOriginalName();
            $ext = $img->getClientOriginalExtension();
            $path = public_path('productimages/thumbnails/'.$imgName);
            Image::make($img->getRealPath())->resize(200, 200)->encode($ext, 75)->save($path);
            
            //insert into thumbnails
            Thumbnail::create([
                'product_id' => $new_product->id,
                'thumbnail' => 'productimages/thumbnails/'.$imgName,
            ]);
        }
        
        $request->images[0]->move('productimages/', $image);
        
        Session::flash('success', 'product added successfully');

        return redirect()->route('admin.products.all');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
            $product = Product::with(['brand','category','subcategory','subsubcategory'])->where('id',$id)->first();
            return json_encode($product);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        
        $product = Product::with(['brand','category','subcategory','subsubcategory','thumbnails'])->where('id',$id)->first();

        return view('admin.products.edit')->with('product', $product)
                                            ->with('cat',Category::all());
                            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

        if ($request->image) {

            //get product image and thumbnails and delete so we can store the new ones
            $proImages = Product::where('id', $id)->first();
            $thumbnails = $proImages->thumbnails;

            unlink($proImages->image);

            foreach($thumbnails as $th){
                unlink($th->thumbnail);
            }


            $this->validate($request, [
                'name'          => 'required',
                'desc'          => 'required',
                'editcat'       => 'required',
                'editsubcat'    => 'required',
                'price'         => 'required|integer',
                'stock'         => 'required|integer',
                'image'         => 'required|max:1024',
            ]);


            $new_image_name = time().'_'.$request->image[0]->getClientOriginalName();


            $update = Product::where('id', $id)->update([
                'name'              => $request->name,
                'description'       => $request->desc,
                'image'             => 'productimages/'.$new_image_name,
                'category_id'       => $request->category,
                'brand_id'          => $request->brand ? $request->brand : null,
                'color'             => $request->color ? $request->color : null,
                'size'              => $request->size  ? $request->size : null,
                'stock'             => $request->stock,
                'price'             => $request->price,
                'category_id'       => $request->editcat,
                'subcategory_id'    => $request->editsubcat,
                'subsubcategory_id' => $request->editsubsubcat ? $request->editsubsubcat : 0,
                'discount'          => $request->discount ? $request->discount : 0
                
                ]);

                $image = $request->image;

                foreach ($image as $img) {
                    $imgName = time().'_'.$img->getClientOriginalName();
                    $ext = $img->getClientOriginalExtension();
                    $path = public_path('productimages/thumbnails/'.$imgName);
                    Image::make($img->getRealPath())->resize(200, 200)->encode($ext, 75)->save($path);
                    
                    //insert into thumbnails
                    Thumbnail::create([
                        'product_id' => $id,
                        'thumbnail' => 'productimages/thumbnails/'.$imgName,
                    ]);
                }

                $request->image[0]->move('productImages/', $new_image_name);

            
        } else {
            $this->validate($request, [
                'name'          => 'required',
                'desc'          => 'required',
                'editcat'       => 'required',
                'editsubcat'    => 'required',
                'price'         => 'required|integer',
                'stock'         => 'required|integer',
            ]);

            Product::where('id', $id)->update([
            'name'              => $request->name,
            'description'       => $request->desc,
            'category_id'       => $request->editcat,
            'subcategory_id'    => $request->editsubcat,
            'color'             => $request->color ? $request->color : null,
            'size'              => $request->size  ? $request->size : null,
            'subsubcategory_id' => $request->editsubsubcat ? $request->editsubsubcat : null,
            'discount'          => $request->discount ? $request->discount : 0,
            'brand_id'          => $request->brand ? $request->brand : null,
            'stock'             => $request->stock,
            'price'             => $request->price,
            ]);
        }
         Session::flash('success', 'product updated successfully');

        return redirect()->route('admin.products.all');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        Session::flash('success', 'product trashed successfully');

        return redirect()->back();
    }

    public function trashed()
    {
        $products = Product::onlyTrashed()->get();

        return view('admin.products.trashed')->with('products', $products);
    }

    public function kill($id)
    {

        $product = Product::withTrashed()->where('id', $id)->first();

        $image = $product->image;
        $thumbnails = $product->thumbnails;

        unlink($image);

        foreach ($thumbnails as $th) {
            $th->delete();
            unlink($th->thumbnail);
        }

        $product->forceDelete();

        Session::flash('success', 'product permanently deleted successfully');

        return redirect()->route('admin.products.all');
    }

    public function restore($id)
    {
        $product = Product::withTrashed()->where('id', $id)->first();

        $product->restore();

        Session::flash('success', 'product restored successfully');

        return redirect()->route('admin.products.all');
    }

    public function status(Request $request)
    {
        switch ($request->type) {
            case 'csh':
                Order::where('id',$request->id)->update([
                    'status' => 'delivered'
                ]);
                Session::flash('success', 'delivery status updated');
        
                return redirect()->back();
                break;

            case'cod':
                
                CashOnDeliveryOrder::where('id',$request->id)->update([
                    'status' => 'delivered'
                ]);
                CashOnDeliveryInvoice::where('cash_on_delivery_order_id',$request->id)->update([
                    'status' => 'paid'
                ]);
                Session::flash('success', 'delivery status updated');
        
                return redirect()->back();

            break;
        }
    }

    public function allOrders()
    {
        $paid = Order::with('user')->orderBy('status')->get();
        $cod = CashOnDeliveryOrder::with('user')->orderBy('status')->paginate(20);
        $codTotal = CashOnDeliveryOrder::with('user')->orderBy('status')->get();

        return view('admin.sales.orders')->with('paid',$paid)
                                        ->with('total',$codTotal)
                                        ->with('cod',$cod);
    }

    public function items(Request $request)
    {
        $ods;

        switch ($request->type) {
            case 'csh':
                $ods = OrderItem::with(['product','user'])->where('order_id',$request->id)->get();

                return view('admin.sales.items')->with('ods',$ods);

                break;
            
            case'cod':
            $ods = CashOnDeliveryOrderItem::with(['product','user'])->where('cash_on_delivery_order_id',$request->id)->get();
            return view('admin.sales.items')->with('ods',$ods);
            break;
        }
    }

    public function customers(Request $request)
    {
        $cus = User::all();

        return view('admin.customers.index')->with('cus',$cus);
        
    }

    public function invoices()
    {
        $paid = Invoice::with('user')->orderBy('status')->get();
        $cod = CashOnDeliveryInvoice::with('user')->orderBy('status')->get();

        $inv = collect([$paid,$cod])->collapse();

        return view('admin.customers.invoices')->with('inv',$inv);
        
    }

    public function unpaid()
    {
        $invs = CashOnDeliveryInvoice::with('user')->get();

        $unpaid = $invs->filter(function($inv){
            return $inv->status === 'unpaid';
        });


        return view('admin.customers.invoices')->with('unpaid',$unpaid);
    }
}
