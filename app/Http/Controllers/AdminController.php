<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Invoice;
use App\Payment;
use App\Product;
use App\CashOnDeliveryOrder;
use App\CashOnDeliveryInvoice;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $prdt = Product::where('stock', '<=', 5)->get();
        
        // cash orders
        $orders = Invoice::with(['user','order'])->orderBy('created_at', 'desc')->get(); 
        
        // get all undelivered cash orders
        $und = $orders->filter(function($item){
            return $item->order['status'] === 'undelivered';
        });
        
        
        //cash on delivery orders
        $cashOnDeliveryOrders = CashOnDeliveryInvoice::with(['user','cash_on_delivery_order'])->orderBy('created_at', 'desc')->get();
        
        // get all undelivered cash on delivery orders
        $cshUnd = $cashOnDeliveryOrders->filter(function($item){
            return $item->cash_on_delivery_order['status'] === 'undelivered';
        });
       
        $totalUndeliveredOrders = $und->count() + $cshUnd->count();
        
        return view('admin.dashboard')->with('usr', User::all())
                                    ->with('allorders', Order::all())
                                    ->with('cod',CashOnDeliveryOrder::all())
                                    ->with('pmt', Payment::all())
                                    ->with('orders', $und)
                                    ->with('csh', $cshUnd)
                                    ->with('undelivred',$totalUndeliveredOrders)
                                    ->with('prdt', $prdt);
    }
}
