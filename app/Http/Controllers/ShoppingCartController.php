<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use Session;
use App\Product;
use Auth;

class ShoppingCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_to_cart(Request $request, $id)
    {

        $itemId = $request->id ? $request->id : $id;

        $prdt = Product::find($itemId);
        

        if($prdt->stock <= 5){
            Session::flash('info', 'product is sold out');

            return redirect()->back();
        }
        
        $prdtItem;

        $VAT = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'VAT 7.2%',
            'type' => 'tax',
            'target' => 'subtotal',
            'value' => '7.2%',
            'attributes' => array(
                'description' => 'Value added tax'
            )
        ));
        
        if ($prdt->discount) {
            $saleDiscount = new \Darryldecode\Cart\CartCondition(array(
                'name' => 'ProductDiscount',
                'type' => 'Discount',
                'value' => -($prdt->discount),
            ));
    
            $prdtItem = array(
                'id' => $prdt->id,
                'name' => $prdt->name,
                'price' => $prdt->price,
                'quantity' => $request->qty ? $request->qty : 1,
                'conditions' => $saleDiscount,
                'attributes' => array(
                    'image'  => $prdt->thumbnails[0]->thumbnail,
                    'discount' => $prdt->discount,
                    'slug' => $prdt->slug
                ),
            );
        } else {
            $prdtItem = array(
                'id' => $prdt->id,
                'name' => $prdt->name,
                'price' => $prdt->price,
                'quantity' => $request->qty ? $request->qty : 1,
                'attributes' => array(
                    'image'  => $prdt->thumbnails[0]->thumbnail,
                    'discount' => 0,
                    'slug' => $prdt->slug
                    
                ),
            );
        }
        
        $cartItem = Cart::add($prdtItem);

        Cart::condition($VAT);

        Session::flash('success', 'product added to cart');

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function incr($id)
    {

        Cart::update($id, array(
            'quantity' =>  1
        ));
        return redirect()->back();
    }

    public function decr($id)
    {
        Cart::update($id, array(
            'quantity' => - 1
        ));
        return redirect()->back();
    }

    public function remove($id)
    {

        Cart::remove($id);

        Session::flash('success', 'product removed from cart');

        return redirect()->back();
    }

    
    public function checkout()
    {

        if (!Auth::check()) {
            return view('shop.signup');
        }

        if (Cart::getContent()->count() === 0) {
            Session::flash('info', 'Your cart is empty!');

            return redirect()->back();
        }
        
        return view('shop.checkout');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $content = Cart::getContent();


        return view('shop.cart')->with('content', $content);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
