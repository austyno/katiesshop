<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Brand;
use App\Category;
use App\SubCategory;
use App\SubsubCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('admin.categories.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.newcat');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'catName' => 'required'
        ]);

        Category::create([
            'name' => $request->catName
        ]);

        Session::flash('success', 'category created successfully');

        return redirect()->route('admin.category.all');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Category::find($id);

        return view('admin.categories.edit')->with('cat', $cat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat = Category::find($id);
        $this->validate($request, [
            'catName' => 'required'
        ]);

        $cat->update(['name' => $request->catName]);

        Session::flash('success', 'category updated successfully');

        return redirect()->route('admin.category.all');
    }

    
    public function createsub()
    {
        $cat = Category::all();
        return view('admin.categories.newsubcat')->with('cat', $cat);
    }

    public function submitsubcat(Request $request)
    {

        $this->validate($request, [
            'subcatname' => 'required',
            'cat' => 'required'
        ]);
        
        SubCategory::create([
            'name' => $request->subcatname,
            'category_id' => $request->cat

        ]);

        Session::flash('success', 'sub category created successfully');

        return redirect()->route('admin.category.all');
    }

    public function all_subcat()
    {
        $allSubCategories = SubCategory::all()->groupBy('category_id')->collapse();


        return view('admin.categories.allsubcat')->with('allSubCategories',$allSubCategories);
    }

    public function edit_subcat($id)
    {
        $subcat = SubCategory::find($id);
        $categories = Category::all();
        return view('admin.categories.editsubcat')->with('subcat',$subcat)
                                                ->with('cat',$categories);
    }

    public function update_subcat(Request $request)
    {
        dd($request->all());

    }

    public function ajaxcat(Request $request)
    {
        
            $id = $request->id;

            //return subcat under this id
            $sub = SubCategory::where('category_id', $id)->get();
            return JSON_encode($sub);
    }

    public function ajaxsubcat(Request $request)
    {
        $id = $request->id;
        //return all subsub cat with this id
        $subsub = SubsubCategory::where('sub_category_id', $id)->get();
        return JSON_encode($subsub);
    }

    public function brands()
    {

        return view('admin.categories.brands')->with('brands', Brand::all());
    }

    public function brandnew()
    {

        return view('admin.categories.brandnew');
    }

    public function brandsave(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'image' => 'required'
        ]);

    $image = $request->image;
    $name = time().'_'.$image->getClientOriginalName();
    $image->move('shop/assets/images/brands/',$name);

        Brand::create([
            'name' => $request->name,
            'image' => 'shop/assets/images/brands/'.$name
        ]);

        Session::flash('success', 'brand created successfully');

        return redirect()->route('admin.category.brands');
    }
}
