<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashOnDeliveryInvoice extends Model
{
    protected $fillable = [
        'user_id','cash_on_delivery_order_id','invoice_number','status','amount'
    ];

    public function cash_on_delivery_order()
    {
        return $this->belongsTo('App\CashOnDeliveryOrder');
    }
    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
