<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thumbnail extends Model
{
    protected $fillable = [
        'product_id','thumbnail'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
