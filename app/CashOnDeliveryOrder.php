<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashOnDeliveryOrder extends Model
{
    protected $fillable = [
        'user_id','invoice_number','qty','total','status','tracking_code'
    ];

    public function orderitems()
    {
        return $this->hasMany('App\CashOnDeliveryOrderItem');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function invoice()
    {
        return $this->belongsTo('App\CashOnDeliveryInvoice');
    }
}
